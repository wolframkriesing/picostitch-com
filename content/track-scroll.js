const renderedTime = new Date();
const scrollOnceFn = () => {
  if ('plausible' in window === false) {
    // Ensure plausible is available, otherwise we can't track the event.
    return;
  }
  
  const scrollDelay = new Date() - renderedTime;
  const scrollDelayMap = {
    50: '0s',
    1000: '0..1s',
    5000: '1..5s',
    Infinity: 'more than 5s'
  };
  const scrolledAfterKey = Object.keys(scrollDelayMap).find(key => scrollDelay <= key);
  const scrolledAfter = scrollDelayMap[scrolledAfterKey];
  
  window.plausible('scrolled-some', {props: {scrolledAfter: scrolledAfter}});
  window.removeEventListener('scroll', scrollOnceFn);
};
window.addEventListener('scroll', scrollOnceFn);
