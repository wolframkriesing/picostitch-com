const navigationItems = [
  {path: '/', name: 'Home 🏠'},
  {path: '/projects', name: 'Projects 🛠‍‍'},
  {path: '/about', name: 'About 💡'},
];
const defaultRenderParams = {
  navigationItems,
  site: { // TODO to be used on the site!
    title: 'picostitch',
    subtitle: 'crafting (and) JavaScript',
    domain: 'picostitch.com',
  },
  blogroll: [
    {url: 'https://blog.thecodewhisperer.com/', name: 'JB Rainsberger', tags: ['good software', 'tdd', 'testing']},
    {url: 'https://michaelfeathers.silvrback.com/', name: 'Michae Feathers', tags: ['good software', 'legacy code']},
    {url: 'https://ruben.verborgh.org/', name: 'Ruben Verborgh', tags: ['solid project', 'privacy', 'decentralized web']},
    {url: 'https://component.kitchen/elix', name: 'Elix - High Quality Web Components', tags: ['project', 'open source', 'web components', 'JavaScript']},
  ]
};

export {navigationItems, defaultRenderParams};
