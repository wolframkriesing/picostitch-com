---
layout: layouts/blog/post.njk
title: "Refactoring Rule of Three"
date: 2014-02-10
dateCreated: "2014-02-10 10:00 CET"
tags:
  - refactoring
  - knowledgebase
postTypes: mini-post
oldUrls: /blog/2014/02/refactoring-rule-of-three/
---


[is a post][18] by [Adrian Bolboaca][@adibolb]

> The rule of three says:
>  
> Extract duplication only when you see it the third time.

[18]: http://blog.adrianbolboaca.ro/2015/02/refactoring-rule-of-three/
[@adibolb]: https://twitter.com/adibolb
