---
layout: layouts/blog/post.njk
title: "Infodeck"
date: 2014-01-20
dateCreated: "2014-01-20 12:00 CET"
tags:
  - slides
postTypes: mini-post
oldUrls: /blog/2014/01/infodeck/
---


Martin Fowler named interactive or more online consumable slides
infodeck, [writes about it here](http://martinfowler.com/bliki/Infodeck.html)
and he [is building a frontend](https://github.com/martinfowler/mfweb)
for it which you can feed via XML.
