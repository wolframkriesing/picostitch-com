---
layout: layouts/blog/post.njk
title: "Building a Parallel Browser"
date: 2014-01-18
dateCreated: "2014-01-18 10:00 CET"
tags:
  - firefox
  - browser engine
  - rust
postTypes: mini-post
oldUrls: /blog/2014/01/building-a-parallel-browser/
---


Servo is an experimental browser engine for modern multi-core hardware written in an experimental memory safe language called Rust.
[Maybe the future Firefox engine?](https://www.youtube.com/watch?v=7q9vIMXSTzc)
