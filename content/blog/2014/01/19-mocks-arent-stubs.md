---
layout: layouts/blog/post.njk
title: "Mocks Aren't Stubs"
date: 2014-01-19
dateCreated: "2014-01-19 10:00 CET"
tags:
  - tdd
  - mock
  - stub
  - testing
privateTags: read later
oldUrls: /blog/2014/01/mocks-arent-stubs/
---


Mocks Aren't Stubs

> The term 'Mock Objects' has become a popular one to describe special case objects that mimic real objects for testing.

Read [the article from 2007](http://martinfowler.com/articles/mocksArentStubs.html)!!!
