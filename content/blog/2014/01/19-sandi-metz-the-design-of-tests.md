---
layout: layouts/blog/post.njk
title: "Sandi Metz - The Design of Tests"
date: 2014-01-19
dateCreated: "2014-01-19 10:00 CET"
tags:
  - testing
  - video
  - mocks
youtubeId: qT5iriwidRg
oldUrls: /blog/2014/01/sandi-metz-the-design-of-tests/
---


Watch the author of [poodr] describing
[how to solve the problem of mocks and real code running out of sync](https://www.youtube.com/watch?v=qT5iriwidRg)!
(And lots of goodies about testing and stuff.)
Watch it twice, it's worth it :)

[poodr]: http://poodr.com
