---
layout: layouts/blog/post.njk
title: "Myth - the simpler CSS transformer?"
date: 2014-01-19
dateCreated: "2014-01-19 17:00 CET"
tags:
  - css
  - preprocessor
oldUrls: /blog/2014/01/myth-the-simpler-css-transformer/
---


[Myth](http://www.myth.io/) - a simpler alternative to all the CSS transformers out there?

It currently has a simple set of features: variables, math, colors, 
simpler media queries and some more. But it looks enough to me.
And it is written in JavaScript, no more ruby gem jams.
