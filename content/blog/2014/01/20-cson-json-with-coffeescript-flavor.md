---
layout: layouts/blog/post.njk
title: "CSON (JSON with CoffeeScript flavor)"
date: 2014-01-20
dateCreated: "2014-01-20 15:00 CET"
tags:
  - npm
  - coffeescript
postTypes: mini-post
oldUrls: /blog/2014/01/cson-json-with-coffeescript-flavor/
---


JSON with CoffeeScript flavor

> CoffeeScript-Object-Notation Parser. Same as JSON but for CoffeeScript objects.

https://www.npmjs.com/package/cson
