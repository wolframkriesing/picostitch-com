---
layout: layouts/blog/post.njk
title: "Workflows of Refactoring"
date: 2014-01-20
dateCreated: "2014-01-20 11:00 CET"
tags:
  - refactoring
  - knowledgebase
postTypes: links
oldUrls: /blog/2014/01/workflows-of-refactoring/
---


Martin Fowler lists the kinds of refactoring, the reasons 
why we refactor code, [in his infodeck](http://martinfowler.com/articles/workflowsOfRefactoring/#2hats).
