---
layout: layouts/blog/post.njk
title: You had one job
tags: 
    - fun
dateCreated: 2014-03-17 10:00 CET
date: 2014-03-17
postTypes: link-list
oldUrls: /blog/2014/03/you-had-one-job/
---

I think this is [a collection of funny accidents][30] that I have to remember, might come in 
handy for a next slide set.

[30]: https://twitter.com/_youhadonejob
