---
layout: layouts/blog/post.njk
title: Fish shell
date: 2014-03-20
dateCreated: 2014-03-20 13:00 CET
tags:
    - shell
    - bash
    - OS
    - tool
postTypes: link-list
oldUrls: /blog/2014/03/fish-shell/
---

[Another tool I have to look at!?][36] Just found via [this article][37].

[36]: http://fishshell.com/
[37]: http://blog.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/
