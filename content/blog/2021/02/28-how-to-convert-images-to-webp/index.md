---
layout: layouts/blog/post.njk
title: "How to Convert Images to webp (and use them)"
date: 2021-02-28
dateCreated: "2021-02-28 13:02 CET"
tags:
    - images
    - website
    - tools
    - images
    - webp
    - tidbit
previewImage: webp-logo.png
slug: how-to-convert-images-to-webp
---

A while ago I optimized some images for the site you are looking at, by using the 
[webp format](https://developers.google.com/speed/webp)
because I had forgotten to do that, and 
[PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/)
suggested to do so.  
I forgot how to do that, so I am writing it down now. Read on if you also want
to know how to convert any kind of image to webp and embed it in your site.

## Install the Executable

I am using a docker setup, so 
[I added](https://github.com/wolframkriesing/site-stitcher/commit/0351946d978a9973ead489233b512d1f6960ba6e)
the following line to my Dockerfile

```
