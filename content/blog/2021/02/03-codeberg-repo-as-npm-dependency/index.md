---
layout: layouts/blog/post.njk
title: "Codeberg Repo as npm Dependency"
date: 2021-02-03
dateCreated: "2021-02-03 00:25 CET"
tags:
    - npm
    - nodejs
    - JavaScript
    - codeberg
    - tidbit
previewImage: codeberg-preview.jpeg
slug: codeberg-repo-as-npm-dependency
---

I am gladly using [codeberg](https://codeberg.org) (as github alternative)
for as many projects as I can, really support their mission.
"Independent and powered by your donations and contributions".  
I struggled a little bit [to figure out the URL that works](#how-it-works)
for a package
that is not on npm yet, but exists just as a codeberg repo.

## Npm Module From a Repo
Npm modules are normally installed via `npm install <package-name>`, but
this works only once the package is published on npm.
If I try to install `npm install test-stitcher`, this fails.

To work with a package straight from the repository one can also use a URL
as dependency, for a codeberg repo it works like this:
`npm install git+https://codeberg.org/wolframkriesing/test-stitcher.git`.
The URL is used as the package name, just the prefix "git+https" was what I had to 
try and play with until I figured it out.

## Failed Tries
I tried a lot of the known URL patterns as dependency in my `package.json`.   
**Those that failed** were:
```
* https://codeberg.org/wolframkriesing/test-stitcher.git
* git://codeberg.org/wolframkriesing/test-stitcher.git
* git@codeberg.org:wolframkriesing/test-stitcher.git
* git+ssh://git@codeberg.org/wolframkriesing/test-stitcher.git
* git+ssh://git@codeberg.org:wolframkriesing/test-stitcher.git
* git+https://codeberg.org/wolframkriesing/test-stitcher.git 
```

Just to quote one error that I received when I called `npm install`: 
```
> npm install
npm ERR! code ENOLOCAL
npm ERR! Could not install from "git@codeberg.org:wolframkriesing/test-stitcher.git" as it does not contain a package.json file.
```

## How it Works

The URL that **works** is prefixed with "git+https":
```
git+https://codeberg.org/wolframkriesing/test-stitcher.git
```

```json
{
  "dependencies": {
    "test-stitcher": "git+https://codeberg.org/wolframkriesing/test-stitcher.git"
  }
}
```

I have not seen and used the combination "git" and "https" before, but hey if 
npm accepts this. Cool. hth
