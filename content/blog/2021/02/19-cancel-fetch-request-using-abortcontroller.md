---
layout: layouts/blog/post.njk
title: "Cancel a fetch request using AbortController"
date: 2021-02-19
dateCreated: "2021-02-19 18:48 CET"
tags:
    - web
    - react
    - fetch
    - JavaScript
    - HTTP
    - tidbit

slug: cancel-fetch-request-using-abortcontroller
---

Finally I learned how to abort a fetch, see the post
[React useEffect and fetch API](https://blog.fullsnackdev.com/post/react-use-effect-and-fetch/)
by [@Jaime](https://twitter.com/jaimefebres).

As easy as this:
```js
const controller = new AbortController();        
// we pass in a `signal` to `fetch` so that we can cancel the requests
fetch('http://example.com', { signal: controller.signal });
controller.abort();
```

Thanks Jaime!
