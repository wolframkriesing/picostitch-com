---
layout: layouts/blog/post.njk
title: "Advices, not only for Rails Projects"
date: 2021-02-27
dateCreated: "2021-02-27 23:11 CET"
tags:
    - architecture
    - software design
    - patterns
    - tidbit

slug: advices-not-only-for-rails-projects
---

[What advice/rules I may give to junior developers about the Ruby on Rails app design?](https://medium.com/@Killavus/what-advice-rules-i-may-give-to-junior-developers-about-the-ruby-on-rails-app-design-6a129bd86b85)
This article is not only relevant for Rails developers, but also
very general. I feel that rails devs need more support here, because
Rails makes it especially hard to use magic.  
Make sure to open the hood and understand how the engine ticks!
