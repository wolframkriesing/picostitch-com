---
layout: layouts/blog/post.njk
title: "Immutable States Done Right with Immer  "
date: 2021-03-16
dateCreated: "2021-03-16 23:46 CET"
tags:
    - JavaScript
    - React
    - state
    - tidbit

slug: immutable-states-done-right-with-immer
---

This blows me away, just like [Zustand](https://github.com/pmndrs/zustand/)
which makes [state management very simple](https://twitter.com/wolframkriesing/status/1371944812253958145)
[immer](https://immerjs.github.io/immer/docs/introduction) implements immutability
in the way it had always wanted to be implemented.

This 6min video explains it and you see what I mean.
https://egghead.io/lessons/redux-simplify-creating-immutable-data-trees-with-immer
