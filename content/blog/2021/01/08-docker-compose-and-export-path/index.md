---
layout: layouts/blog/post.njk
title: "docker-compose and Export $PATH"
date: 2021-01-08
dateCreated: "2021-01-08 17:02 CET"
tags:
    - docker
    - tools
    - docker-compose
    - containers
    - tidbit
previewImage: export-path-preview.jpeg
slug: docker-compose-and-export-path
---

[All my projects use docker-compose](/blog/2021/01/08-docker-vs-docker-compose/).
In my setup I want every nodejs packages that provides an executable, such as `mocha` or `tsc`
to be available globally on the command line.

So I can type
```shell-session
$ mocha
...
```

I don't want to have to type the long version:

```shell-session
$ ./node_module/.bin/mocha
...
```

## How to Expose nodejs Executables?

I don't want to `cd` into the project directory
and call the executable via `./node_module/.bin/mocha` or `./node_module/.bin/tsc` from there.
I can set this up by extending my machine's path like this `PATH=$PATH:./node_modules/.bin`.

The [according `Dockerfile`](https://github.com/wolframkriesing/site-stitcher/blob/944038f503d8ae47c12de1982141dc8d9df6937a/Dockerfile#L3) looks like this:
```dockerfile
FROM node
ENV PATH=$PATH:./node_modules/.bin
```

## Define Path Export in docker-compose

I was searching the [docker-compose docs](https://docs.docker.com/compose/compose-file/compose-file-v3/) but I could not find how to add a path to the existing path
inside a docker-compose setup. The typical docker-compose setup is stored in a `docker-compose.yml`
file and for nodejs it might look like this (very simplified):
```yaml
version: '3'
services:
  picostitch:
    image: nodejs
    ports:
      - "5000:5000"
    volumes:
      - '.:/app'    
```
see the [complete file running this blog, here](https://github.com/wolframkriesing/site-stitcher/blob/76ba1541673a156d5cd944241ea7f29200fffeb5/docker-compose.yml).

In a docker-compose you can configure (multiple) services. The one I need here is `picostitch`, which is
just a simple nodejs container, it exposes the port 5000 and maps the local directory `.` into the container
under `/app`.

Beside `ports`, `volumes` docker-compose also provides `environment` where one can define environment variables
to be set inside the running container. But I did not find a way to export a path.

## Use docker-compose's `build` Option

In this case I used the `build` option, which takes a directory where it looks for a `Dockerfile`, in which I set the 
path accordingly.
```yaml
version: '3'
services:
  picostitch:
    build: .
```

In the end I do use exactly the `Dockerfile` as mentioned above, so **docker-compose** can use it to build
my **picostitch** service.

```dockerfile
FROM node:15
ENV PATH=$PATH:./node_modules/.bin
```

See the [files in the repo](https://github.com/wolframkriesing/site-stitcher/tree/944038f503d8ae47c12de1982141dc8d9df6937a);

## Multiple Containers

When I have multiple containers that need a customized Dockerfile, I create a directory `docker-files/<service-name>`
where I put the `Dockerfile` in each directory. **docker-compose** also needs that for building the image in that
directory and use it as it's context.
