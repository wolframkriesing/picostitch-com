---
layout: layouts/blog/post.njk
title: "Six Working Flexbox Layouts"
date: 2021-01-25
dateCreated: "2021-01-25 22:26 CET"
tags:
    - css
    - flexbox
    - tidbit
previewImage: flexbox-preview.jpeg
slug: six-working-flexbox-layouts
---

Layouting a site with CSS is initially easy. It becomes fun and more difficult when
it shall become responsive, when you have a special design in mind and when one browser
does not work as expected.  
The site [Solved by Flexbox](https://philipwalton.github.io/solved-by-flexbox/)
by [@philwalton](https://twitter.com/philwalton)
is a great resource where he shows six major use cases and describes
the solutions.  

Thanks a lot Phil, great job!

<figure>
    <img src="flexbox-browser-support.jpeg" alt="The best, a lot of browsers are covered." width="100%" />
    <figcaption>The best, a lot of browsers are covered.</figcaption>
</figure>

[Visit: "Solved by Flexbox" it is worth it!](https://philipwalton.github.io/solved-by-flexbox/)
