---
layout: layouts/blog/post.njk
title: "Learning ReScript - Part 2 (rescript Command)"
date: 2021-05-22
dateCreated: "2021-05-22 22:36 CET"
tags:
    - ReScript
    - JavaScript
    - typed language
    - typing
    - learning
    - tidbit
previewImage: rescript-cutout.gif
slug: learning-rescript-part-2
---

In [Part 1](../22-learning-rescript-part-1) I started with ReScript,
I set up [a project](https://codeberg.org/wolframkriesing/rescript-learning-commit-by-commit) 
and made my first steps, got it running.  
Now I want to learn more and get to the point where I have a feeling for how to use ReScript, 
lets see where it takes me. I guess it will take me more than this "Part 2" post to be
comfortable to start a project with ReScript.

## Contents

1) [Contents](#contents)
1) [Automate Building and Running](#automate-building-and-running)
1) [The rescript CLI Command](#the-rescript-cli-command)
1) [Build and Run](#build-and-run)
1) [First Language Learning - Let Binding](#first-language-learning---let-binding)
    1) [Block Scope](#block-scope)
    1) [Block Scope Compiles to ...](#block-scope-compiles-to-)
    1) [Shadowing](#shadowing)
    1) [Make it a Module](#make-it-a-module)
1) [To be Continued ...](#to-be-continued-)

## Automate Building and Running

I want to have one command that builds the files and runs them, e.g. `npm start` or `npm run build` or alike.
Best would be of course, that it does that continuously in the background.

Before automation that I like to understand how to compile and run the compiled file.

## The `rescript` CLI Command

Until now running `rescript` did the compilation. It generated the `hello.bs.js` file in the
same directory as where the `hello.res` file is located, in `src/`.

For me the rescript command is still a black box. I don't want to understand it down to the
implementation (yet), but I want to know it a bit better. What can it do? Am I using it right?
Why do I just call `rescript` and not e.g. `rescript compile`?
So I let `rescript` tell me what it can tell to help me, via `rescript --help`.
```shell-session
