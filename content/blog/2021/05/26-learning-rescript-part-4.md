---
layout: layouts/blog/post.njk
title: "[WIP] Learning ReScript - Part 4 (Tuples)"
date: 2021-05-26
dateCreated: "2021-05-26 12:39 CET"
tags:
    - ReScript
    - JavaScript
    - typed language
    - typing
    - learning
    - tidbit

slug: learning-rescript-part-4
---

isDraft: true  

seen formatting and first type annotations in part 3, now lets get a bit more concrete with tuples
real data structures ftw
