---
layout: layouts/blog/post.njk
title: "Learning ReScript - Part 3 (Format, Start Typing)"
date: 2021-05-24
dateCreated: "2021-05-24 16:41 CET"
tags:
    - ReScript
    - JavaScript
    - typed language
    - typing
    - learning
    - tidbit
previewImage: rescript-cutout2.gif
slug: learning-rescript-part-3
---

The setup is working, I can compile and run ReScript files, I touched a bit
on the syntax in [Part 2](../22-learning-rescript-part-2), next I will continue through 
the docs chapters from top to bottom.

<figure>
    <img src="rescript-docs-toc.gif" alt="ReScript docs TOC" width="100" class="sizeup-onhover-image scale2 origin-left-top" />
    <figcaption>ReScript docs TOC</figcaption>
</figure>

## Contents

1) [Formatting](#formatting)
1) [First Type Annotations](#first-type-annotations)
1) [Type Alias](#type-alias)
1) [Retrospective #1](#retrospective-1)
1) [An Opinion on the Docs](#an-opinion-on-the-docs)
1) [To be Continued ...](#to-be-continued-)

## Formatting

I remember having heard on the [podcast with Patrick Ecker](https://changelog.com/jsparty/175)
that ReScript comes with a formatting tool (just like golang, or many of the newer languages),
which prevents discussions about code layout, cool. Now it's time to apply it, the hello world 
code is growing, it needs some proper styling.

After `rescript -h` (remember, not `--help`!) I figured out it is `rescript format`.
Alright. I run `rescript format`, nothing happens. So I have to pass the filename explicitly.
Sad. Ok, `rescript format src/hello.res` works fine. It took me a while to read the `rescript format -h` 
help page properly to figure out that
```shell-session
