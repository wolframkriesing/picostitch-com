---
layout: layouts/blog/post.njk
title: "Cohesion and Coupling explained"
date: 2015-05-11
dateCreated: "2015-05-11 13:00 CET"
tags:
  - oop
  - coupling
  - cohesion
  - classes
  - video
  - knowledgebase
postTypes: mini-post
privateTags: must watch
youtubeId: XCXdsjINP0U
oldUrls: /blog/2015/05/cohesion-and-coupling-explained/
---


Wikipedia says about ***cohesion***, it [*refers to the degree to which the elements of a module belong together*][cohesion].  
And about ***coupling*** it says [*how closely connected two routines or modules are*][coupling].

See below for a [very good short 6min video][58] explaining how and why you should aim for high cohesion and loose coupling!  
And if you want to spend more time watch [this video 57min][59] with all the stars :) discussing this topic more in detail.

[58]: https://www.youtube.com/watch?v=XCXdsjINP0U
[59]: https://www.youtube.com/watch?v=hd0v72pD1MI
[cohesion]: https://en.wikipedia.org/wiki/Cohesion_(computer_science)
[coupling]: https://en.wikipedia.org/wiki/Coupling_(computer_programming)
