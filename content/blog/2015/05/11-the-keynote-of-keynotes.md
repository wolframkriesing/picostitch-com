---
layout: layouts/blog/post.njk
title: "The keynote of keynotes"
date: 2015-05-11
dateCreated: "2015-05-11 12:00 CET"
tags:
  - video
postTypes: mini-post
youtubeId: oKg1hTOQXoY
oldUrls: /blog/2015/05/the-keynote-of-keynotes/
---


[Alan Kay at OOPSLA 1997 - The computer revolution hasnt happened yet](https://www.youtube.com/watch?v=oKg1hTOQXoY)

The notes for this talk can be found at 
http://www.cc.gatech.edu/fac/mark.guzdial/squeak/oopsla.html and in the c2 wiki at 
http://c2.com/cgi/wiki?TheComputerRevolutionHasntHappenedYet
