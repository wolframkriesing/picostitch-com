---
layout: layouts/blog/post.njk
title: "Typeslab"
date: 2015-04-13
dateCreated: "2015-04-13 13:47 CET"
tags:
  - for slides
  - poster
  - typography
postTypes: link-list
oldUrls: /blog/2015/04/typeslab/
---


With [typeslab.com][47] you can write poster like text.
  
![for slides maybe?][46]

[46]: http://i.imgur.com/CpDxngp.png
[47]: http://typeslab.com/
