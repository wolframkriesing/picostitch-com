---
layout: layouts/blog/post.njk
title: "Estimation is evil"
date: 2015-10-06
dateCreated: "2015-10-06 09:00 CET"
tags:
  - estimation
  - agile
  - article
postTypes: mini-post
privateTags: must read
oldUrls: /blog/2015/10/estimation-is-evil/
---


[A long article][60] by [@RonJeffries] about "overcoming the estimation obsession".

[@RonJeffries]: https://twitter.com/RonJeffries
[60]: https://pragprog.com/magazines/2013-02/estimation-is-evil
