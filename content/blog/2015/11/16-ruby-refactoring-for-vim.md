---
layout: layouts/blog/post.njk
title: "Ruby refactoring for vim"
date: 2015-11-16
dateCreated: "2015-11-16 14:03 CET"
tags:
  - ruby
  - refactoring
---


I [found this repo][rubyrefactoring-repo], which seems not to be the newest, but one (if not the only one) 
which provides some ways of refactoring for ruby.

[rubyrefactoring-repo]: https://github.com/ecomba/vim-ruby-refactoring
