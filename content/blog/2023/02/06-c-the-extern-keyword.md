---
layout: layouts/blog/post.njk
title: "C the `extern` keyword"
date: 2023-02-06
dateCreated: "2023-02-06 13:56 CET"
tags:
    - C
    - embedded
    - programming language
    - tidbit

slug: c-the-extern-keyword
---

I just had the error `src/main.c:46: multiple definition of '_led'` again.
I figured the `extern` keyword is coming for my rescue.

## What Failed?
In `*.h` files I wrote `const Led led;` to "announce" that the
according C file provides this variable. But if multiple places `#include ...`
this header file this clashes.

Of course one can surround the entire header file with `#ifndef MAKE_UP_A_NAME`,
but to me this feels like a symptom for hiding something we can do better.

## The Solution
So I discovered `extern`. And by doing so I also learned why `const Led led;`
is just not a good idea.

```c
int var;  ->  declaration and definition
extern int var;  -> declaration
```

In short `extern int var;` just announces that `var` will be defined at some point in time.
In comparison `int var;` does define `var` already, which is not what we want, because if
that happens twice the compiler complains. Why? Because all code, from all files 
can be imagined as getting thrown into one bit file and then there should 
be no duplicates definitions of anything.
