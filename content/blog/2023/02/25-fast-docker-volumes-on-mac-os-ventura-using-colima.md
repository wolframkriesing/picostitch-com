---
layout: layouts/blog/post.njk
title: "Fast Docker Volumes on MacOS Ventura using Colima"
date: 2023-02-25
dateCreated: "2023-02-25 14:55 CET"
tags:
    - docker
    - colima
    - macos
    - tidbit

slug: fast-docker-volumes-on-mac-os-ventura-using-colima
---

I had sync delays of about 15 seconds, when I changed a file on my MacOS fs and that it was
seen as changed inside the docker container. This solved it: `colima start --mount-type 9p`

I found this here https://github.com/abiosoft/colima/issues/500#issuecomment-1343103477
