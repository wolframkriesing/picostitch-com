---
layout: layouts/blog/post.njk
title: "JS (and) Crafting #12 – Modern JS Workshop, Static Initialization Block, Private Class Fields, GoodJobs and Nicola Pellow"
date: 2023-12-18
dateCreated: "2023-12-18 23:00 CET"
tags:
  - newsletter
  - workshop
  - ecmascript
  - learning
  - jobs
  - browser
isNewsletter: true
---


This is JS (and) Crafting #11 - December 18th, 2023. Every Monday you will receive a hand-selected collection of **links about JavaScript and how to craft better software**.

But before the content start, I have one personal thing.

## I start giving Workshops in 2024

I am working with JavaScript for more than 24 years now, I am a test advocate and with the jskatas, the JSCraftCamp
and the #jslang meetup, I try to share my knowledge with as many people as possible. Now I am starting the next level.

Together with the alm.sh I will be starting to give workshops on JavaScript and adjacent topics.
I will start with six workshops in February, March and April 2024.
If you can convince your company, bring a fellow developer and get a 10% discount.  
[👉 👉 👉 Check out my "Modern JavaScript Development" workshop](https://www.alm.sh/academy/modern-javascript).  
You have any special questions or wishes? [Get in contact directly](mailto:w+workshops@kriesing.de).

## JavaScript

### Static Initialization Blocks  
In last week's [JavaScript the Language Meetup](https://www.meetup.com/javascript-the-language/) 
we wrote some code to explore static initialization blocks.
I guess [this test](https://codeberg.org/wolframkriesing/jslang-meetups/src/branch/main/static-init-block-2023-12-13/test.js#L79-L92)
explains best what a static initialization block is.  
Somehow I have the impression this is a feature that won't be used much.
Anyway, it looks like this:
```js
class Klass{
  static {
    // code that runs once, when the class is loaded
  }
}
```

### Private Class Fields is huge: 20 new Chapters in the Spec
With ES2022 (ES13) private class fields have been introduced.
If one searches the [ES2022 specification](https://262.ecma-international.org/13.0/) for "private" there are 38 hits.
in the [ES2021 spec](https://262.ecma-international.org/12.0/) there had been only 18 hits, and they are about 
built-ins such as Array, DataView, Atomics, so they do not look like anything related to classes.
This means that 20 chapters (every hit is a chapter) cover the topics of private fields in classes 🤯 wow, 
this feels like a lot (of work). Thanks to all the [proposal authors and contributors](https://github.com/tc39/proposal-class-fields/graphs/contributors).
If I looked right it was [Daniel Ehrenberg](https://github.com/tc39/proposal-class-fields/commit/95718abe870c3bd5cac2bf155f76c879458e9b4e)
initiating and working a lot on the proposal, starting in 2017.


## Crafting

### Find a good job at GoodJobs!
[GoodJobs](https://goodjobs.eu/) looks like a sustainability-focused job search. 
Their [criteria page](https://goodjobs.eu/criteria) describes in detail what companies have to fulfill to get listed.
In short GoodJobs has established 5 sustainability categories based on the [17 Sustainable Development Goals of the United Nations](https://sdgs.un.org/goals).

### Nicola Pellow Wrote the First Cross-Plattform Web Browser, in 1990
[Nicola Pellow, in 1990 a math student intern working at CERN](https://en.wikipedia.org/wiki/Nicola_Pellow) 
implemented the first cross-platform web browser in C, since Tim Berners-Lee browser for the NeXT machine was not portable.
This browser was called the [Line Mode Browser](https://www.w3.org/LineMode) (LMB, WWWLib, or just www) 
it only displayed text, no images and not even any need for JavaScript.
It was command line based, [the command "list"](https://www.w3.org/LineMode/User/Commands.html) 
gave a numbered list of the links for the current website

<br><br><br>

## Also Have a Look at

[jskatas.org](https://jskatas.org) - Learn JavaScript – Test-Driven.  
["JavaScript the Language" Meetup](https://www.meetup.com/JavaScript-The-Language/) - A team-coding, no-presentations meetup.  
[The #jslang Repo](https://codeberg.org/wolframkriesing/jslang-meetups) - Tests and learnings from 55+ #jslang meetups.   

## Feedback?

I'm eager to hear your thoughts, input, corrections or interesting links. Please get in touch via [mastodon](https://mastodontech.de/@wolframkriesing)
provide a [pull request](https://codeberg.org/wolframkriesing/) or [email me](mailto:w+js-and-crafting-newsletter@kriesing.de).

Happy Crafting!

Wolfram