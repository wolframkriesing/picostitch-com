---
layout: layouts/blog/post.njk
title: "JS (and) Crafting #11 – Grace Hopper, FinalizationRegistry, escape() and unescape(), CSS Nesting"
date: 2023-12-11
dateCreated: "2023-12-11 23:00 CET"
tags:
  - newsletter
  - ecmascript
  - css
  - javascript
isNewsletter: true
---


This is JS (and) Crafting #11 - December 11th, 2023. Every Monday you will receive a hand-selected collection of **links about JavaScript and how to craft better software**.

## JavaScript

**Callback when a Value gets Garbage-Collected**  
[FinalizationRegistry](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/FinalizationRegistry) 
provides a way to request that a cleanup callback get called at some 
point when a value registered with the registry has been reclaimed (garbage-collected). 
(Cleanup callbacks are sometimes called finalizers.)
This was introduced in ES2021 (ES12) in June 2021.

**Global Functions `escape()` and `unescape()` Were Deprecated in ES3**  
[escape()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/escape)
and [unescape()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/unescape)
were removed in ES3 (1999) and are not part of the ECMAScript standard anymore,
instead use any of the functions `encodeURI()`, `encodeURIComponent()`, `decodeURI()`, `decodeURIComponent()`.

## Crafting

**Grace Hopper**  
[Grace Hopper](https://en.m.wikipedia.org/wiki/Grace_Hopper) was a computer scientist and US Navy rear admiral. 
She was a pioneer of computer programming who invented one of the first linkers. 
She popularized the idea of machine-independent programming languages, which led to the development of COBOL, 
an early high-level programming language still in use today.
> She believed that programming should be simplified with an English-based computer programming language. Her compiler converted English terms into machine code understood by computers.

**CSS Nesting**  
Finally I can write CSS (almost) as I always did, [with nesting](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_nesting/Using_CSS_nesting).
I used to write CSS like this:
```css
p {}
  p a {}
```
With nesting I can write it like this:
```css
p {
  a {}
}
```
I love it.




<br><br><br>

## Also Have a Look at

[jskatas.org](https://jskatas.org) - Learn JavaScript – Test-Driven.  
["JavaScript the Language" Meetup](https://www.meetup.com/JavaScript-The-Language/) - A team-coding, no-presentations meetup.  
[The #jslang Repo](https://codeberg.org/wolframkriesing/jslang-meetups) - Tests and learnings from 55+ #jslang meetups.   

## Feedback?

I'm eager to hear your thoughts, input, corrections or interesting links. Please get in touch via [mastodon](https://mastodontech.de/@wolframkriesing)
provide a [pull request](https://codeberg.org/wolframkriesing/) or [email me](mailto:w+js-and-crafting-newsletter@kriesing.de).

Happy Crafting!

Wolfram