---
layout: layouts/blog/post.njk
title: "JS (and) Crafting #10 – ECMAScript Specification Growth, CSS Math Functions, Tota11y Bookmarklet"
date: 2023-12-04
dateCreated: "2023-12-04 20:51 CET"
tags:
  - newsletter
  - ecmascript
  - css
  - accessibility
isNewsletter: true
---


This is JS (and) Crafting #10 - December 4th, 2023. Every Monday you will receive a hand-selected collection of **links about JavaScript and how to craft better software**.

## JavaScript

**Four Statements added in ECMAScript Version 3**  
In 1999, the then new ECMAScript version ES3 got four additions to its 
statements 1) do-while 2) switch 3) throw 4) try.
Did these do good to our coding? Imagine if try would have never been added.  
[Originally posted here](https://mastodontech.de/@wolframkriesing/111492812810864717).

**ECMAScript Specification Growth**  
My inexact word count of the ECMAScript Specification since [version 5.1 (2011)](https://262.ecma-international.org/5.1) 
to the latest [version 13 (2023)](https://262.ecma-international.org/13.0/) 
has increased by 319.353 words, which is a 57% increase.
How did I count words? Open one of the spec URLs and run in your console `$('body').textContent.split(' ').length`.  
[Originally posted here](https://mastodontech.de/@wolframkriesing/111495184049023753).

## Crafting

**CSS has Math Functions**  
Beside most common one `calc()` there are
functions like `min()`, `max()`, `clamp()`, `round()`, `sin()`, `cos()` and 
[many more](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Functions#math_functions).
Which allows to write CSS like this `width: min(100%, round(var(--my-value)));`.

**Accessibility Tool Highlight: Tota11y Bookmarklet**  
[A11y Bookmarklet Tota11y](http://brucelawson.github.io/tota11y/) "is a simple tool to visualise the most 
widespread web accessibility errors in a non overwhelmingly-techy way".  
[I learned this here](https://mastodontech.de/@jensgro@mastodon.social/111522683408791613).

<br><br><br>

## Also Have a Look at

[jskatas.org](https://jskatas.org) - Learn JavaScript – Test-Driven.  
["JavaScript the Language" Meetup](https://www.meetup.com/JavaScript-The-Language/) - A team-coding, no-presentations meetup.  
[The #jslang Repo](https://codeberg.org/wolframkriesing/jslang-meetups) - Tests and learnings from 55+ #jslang meetups.   

## Feedback?

I'm eager to hear your thoughts, input, corrections or interesting links. Please get in touch via [mastodon](https://mastodontech.de/@wolframkriesing)
provide a [pull request](https://codeberg.org/wolframkriesing/) or [email me](mailto:w+js-and-crafting-newsletter@kriesing.de).

Happy Crafting!

Wolfram