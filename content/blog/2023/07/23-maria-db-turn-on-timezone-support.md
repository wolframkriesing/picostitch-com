---
layout: layouts/blog/post.njk
title: "How to turn on Timezone Support for MariaDB?"
date: 2023-07-23
dateCreated: "2023-07-23 17:11 CET"
tags:
    - mariadb
    - db
    - timezone
    - tidbit

slug: maria-db-turn-on-timezone-support
---

If you see no entries in MariaDB's time_zone table, then you have no timezone support.
```sql
select * from mysql.time_zone;
Empty set (0.001 sec)
```

## Why turn on MariaDB Timezone Support?

That means that queries like:
```sql
> select CONVERT_TZ('2023-01-01', 'UTC', 'Europe/Berlin');
+--------------------------------------------------+
| CONVERT_TZ('2023-01-01', 'UTC', 'Europe/Berlin') |
+--------------------------------------------------+
| NULL                                             |
+--------------------------------------------------+
1 row in set (0.000 sec)
```

Will return `NULL` which means that queries that use `CONVERT_TZ` will not work.
This is for example the case with some queries that [Django](https://djangoproject.com) creates, like a query like this
```python
>>> User.objects.filter(date_joined__year=2023).only('date_joined').values_list('date_joined')
<QuerySet [(datetime.datetime(2023, 7, 22, 19, 37, 48, tzinfo=datetime.timezone.utc),)]>
>>> User.objects.filter(date_joined__month=7)
<QuerySet []> 
```

Filtering for a year works, but filtering for a month does not. Because under the hood, django 
creates a query like this:
```sql
