---
layout: layouts/blog/post.njk
title: "`cat` a file in reverse"
date: 2023-07-23
dateCreated: "2023-07-23 17:57 CET"
tags:
    - linux
    - command line
    - shell
    - tidbit

slug: cat-a-file-in-reverse
---

You know `cat file.txt` which prints all the lines in a file from first to last.
But what if you want to print them from **last to first**?
Easy, just write `cat` backwards, which is `tac` voila.
```bash
tac file.txt
```

I learned this from ChatGPT, iirc.
