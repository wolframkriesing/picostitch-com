---
layout: layouts/blog/post.njk
title: "Strict Types in C99"
date: 2023-07-20
dateCreated: "2023-07-20 19:10 CET"
tags:
    - C
    - C99
    - embedded
    - types
    - tidbit

slug: strict-types-in-c99
---

isDraft: true

not really possible,   
but use speaking types e.g. `typedef uint8_t bool_t;`
