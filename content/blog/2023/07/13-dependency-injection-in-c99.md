---
layout: layouts/blog/post.njk
title: "Dependency Injection in C99"
date: 2023-07-13
dateCreated: "2023-07-13 12:59 CET"
tags:
    - C
    - C99
    - embedded
    - testing
    - tidbit

slug: dependency-injection-in-c99
---

isDraft: true

does not really exist in c  
what to inject?  
- singletons, like state that is unique throughout the project, e.g. system state (on, off, booting, shutting down, ...)
- modules/functions as pointers
