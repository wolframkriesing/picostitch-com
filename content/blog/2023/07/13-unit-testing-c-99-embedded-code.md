---
layout: layouts/blog/post.njk
title: "Unit Testing C99 Embedded Code"
date: 2023-07-13
dateCreated: "2023-07-13 12:59 CET"
tags:
    - C
    - C99
    - embedded
    - testing
    - tidbit

slug: unit-testing-c-99-embedded-code
---

isDraft: true

post topics:
- make it pure C, no HW dependencies  
- run the tests with gcc, no HW specific code  
- maybe needs a fake/mock library (like q16)
