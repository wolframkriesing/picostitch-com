---
layout: layouts/blog/post.njk
title: "Modern Dev Stuff applied to C99 Development"
date: 2023-07-13
dateCreated: "2023-07-13 12:50 CET"
tags:
    - C
    - C99
    - development
    - tidbit

slug: modern-dev-stuff-applied-to-c99-development
---

isDraft: true

ideas:
- small commits
- merge requests
- code in public, commit and push often, share often with the colleagues
- use CI
- write unittests
  - abstract away the HW part
  - test the pure C parts
  - this creates architecture
- use ADRs
