---
layout: layouts/blog/post.njk
title: "Hacker Movies Watch List"
date: 2023-09-18
dateCreated: "2023-09-18 12:18 CET"
tags:
    - movies
    - hacker
    - culture
    - tidbit

slug: hacker-movies-watch-list
---

When I listened to the [CRE podcast about hacker movies (german only)](https://cre.fm/cre202-hackerfilme)
I opened a lot of tabs in my browser which I never closed again.
So now it's time to just simply persist all the links to the suggested hacker movie
that had been mentioned in the podcast.

- "Welt am Draht" (1973): [wikipedia](https://de.wikipedia.org/wiki/Welt_am_Draht), 
  [Der Film - youtube](https://www.youtube.com/watch?v=TP-kjksRbaE) Rainer Werner Fassbinder,
  [US Trailer](https://www.youtube.com/watch?v=URq7m3-SOtA)
- "WarGames" (1983): [Trailer](https://www.youtube.com/watch?v=TQUsLAAZuhU)
- "Assassins" (1995): [Trailer](https://www.youtube.com/watch?v=XCuD8Q_Y10Q)
- "Takedown" (2000): [Official Trailer](https://www.youtube.com/watch?v=NbgDMYy9mzM)
- "23 - Nichts ist so, wie es scheint" (1998): [Trailer, deutsch](https://www.youtube.com/watch?v=JUDWU4RBtds)
- "Revolution OS" (2001): [full documentary](https://www.youtube.com/watch?v=jw8K460vx1c), 
  "... documentary film that traces the twenty-year history of GNU, Linux, open source, and the free software movement." [wikipedia says](https://en.wikipedia.org/wiki/Revolution_OS)
- "Alles ist Eins. Außer der 0." (2020) [Documentary](https://www1.wdr.de/fernsehen/wdr-dok/alles-ist-eins-ausser-der-0-clip-alles-ist-eins-ausser-der-0--100.html),
  [imdb site](https://www.imdb.com/title/tt11794022/).
  The story of the legendary “Chaos Computer Club” (CCC) and its founding soul Wau Holland (Dr. Wau).
