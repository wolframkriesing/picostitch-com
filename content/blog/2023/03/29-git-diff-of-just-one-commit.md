---
layout: layouts/blog/post.njk
title: "Git Diff of just one Commit"
date: 2023-03-29
dateCreated: "2023-03-29 09:13 CET"
tags:
    - git
    - tidbit

slug: git-diff-of-just-one-commit
---

I was wondering how I can see the change of one git commit, I want to see ONLY the 
changes of this commit, not compared to any other or alike, found it:
`git diff <hash>~ <hash>`.
Welcome in git wonderland :).
