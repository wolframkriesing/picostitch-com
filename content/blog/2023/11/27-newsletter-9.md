---
layout: layouts/blog/post.njk
title: "JS (and) Crafting #9 – ECMAScript Version 1, Static Initialization Blocks, roc-lang, minimal.gallery"
date: 2023-11-27
dateCreated: "2023-11-27 11:51 CET"
tags:
  - newsletter
  - roc-lang
  - elm
  - javascript
isNewsletter: true
---


This is JS (and) Crafting #9 - November 27th, 2023. Every Monday you will receive a hand-selected collection of **links about JavaScript and how to craft better software**.

## JavaScript

[***The 1st version of the ECMAScript specification*** (PDF 327kB)](https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf) from 1997,
a 110-page document that lays the foundation for what JavaScript has become today. A must-read for enthusiasts and professionals alike.

[***Static initialization blocks are new in JavaScript*** (on MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/Static_initialization_blocks). 
While I'm still forming an opinion on this feature, as [discussed here](https://mastodontech.de/@wolframkriesing/111471311404387226), 
it's undoubtedly a candidate for a [jskatas.org](https://jskatas.org) update.

## Crafting

[***A Taste of Roc 📺 Richard Feldman introduces the direct descendant of Elm*** ](https://www.youtube.com/watch?v=6qzWm_eoUXM),
which promises to be a "fast, friendly, functional language".
The site has a WebAssembly-powered REPL, [see roc-lang.org](https://roc-lang.org/).

[***minimal.gallery has well-crafted, visually expressive websites***](https://minimal.gallery/).
A standout example for me is [only8.org](https://only8.org/), interesting facts about Barcelona's street names. 
Aesthetic design and meaningful content that resonates with our times.

<br><br><br>

## Also Have a Look at

[jskatas.org](https://jskatas.org) - Learn JavaScript – Test-Driven.  
["JavaScript the Language" Meetup](https://www.meetup.com/JavaScript-The-Language/) - A team-coding, no-presentations meetup.  
[The #jslang Repo](https://codeberg.org/wolframkriesing/jslang-meetups) - Tests and learnings from 55+ #jslang meetups.   

## Feedback?

I'm eager to hear your thoughts, input, corrections or interesting links. Please get in touch via [mastodon](https://mastodontech.de/@wolframkriesing)
provide a [pull request](https://codeberg.org/wolframkriesing/) or [email me](mailto:w+js-and-crafting-newsletter@kriesing.de).

Happy Crafting!

Wolfram