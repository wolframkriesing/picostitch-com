---
layout: layouts/blog/post.njk
title: "EventSource: Native Browser Streaming API"
date: 2024-09-06
dateCreated: "2024-09-06 18:55 CET"
tags:
    - JavaScript
    - EventSource
    - webdev
    - tidbit

slug: eventsource-native-browser-streaming-api
---

I wrote this post over on hashnode https://picostitch.hashnode.dev/eventsource-native-browser-streaming-api
