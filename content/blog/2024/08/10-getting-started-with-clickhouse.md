---
layout: layouts/blog/post.njk
title: "Getting Started with Clickhouse"
date: 2024-08-10
dateCreated: "2024-08-10 19:54 CET"
tags:
    - ClickHouse
    - DB
    - tidbit

slug: getting-started-with-clickhouse
---

Ever heard of OLAP?
If you want to handle millions of rows and run analytics on these data, you might want to look into ClickHouse.
I wrote over on hashnode about it:
https://picostitch.hashnode.dev/getting-started-with-clickhouse
