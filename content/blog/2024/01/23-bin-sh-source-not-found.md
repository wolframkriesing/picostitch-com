---
layout: layouts/blog/post.njk
title: "How to fix the error \"/bin/sh: 1: source: not found\"?"
date: 2024-01-23
dateCreated: "2024-01-23 22:55 CET"
tags:
    - docker
    - linux
    - shell
    - bash
    - source
    - tidbit

slug: bin-sh-source-not-found
---

I am installing nodejs inside a docker container and have this line in the Dockerfile:
```dockerfile
RUN mkdir -p $NVM_DIR
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
RUN source $NVM_DIR/nvm.sh
```

But when I run the container I get this error:
```
 => ERROR [node base 11/15] RUN source /usr/local/nvm/nvm.sh                                                                                                                          0.2s

 > [node base 11/15] RUN source /usr/local/nvm/nvm.sh:
#0 0.227 /bin/sh: 1: source: not found             # <<<<<<<<<<<<<<< the error

failed to solve: executor failed running [/bin/sh -c source $NVM_DIR/nvm.sh]: exit code: 127
```

## What causes the error `source: not found`?

Since `bash` is installed in the container, I suspected that the `/bin/sh` prefix means
that bash is not used as shell. After a while I found out how to set the shell to bash:
```dockerfile
SHELL ["/bin/bash", "-c"]  # <<<<<<<<< add this line
RUN mkdir -p $NVM_DIR
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
RUN source $NVM_DIR/nvm.sh
```

The `SHELL ["/bin/bash", "-c"]` line tells docker to use bash as shell and the `-c` tells
bash to run the following command. So now the `source` command is found and the error is fixed.
