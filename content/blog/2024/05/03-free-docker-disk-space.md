---
layout: layouts/blog/post.njk
title: "Free Docker Disk Space"
date: 2024-05-03
dateCreated: "2024-05-03 16:00 CET"
tags:
    - docker
    - linux
    - free
    - cleanup
    - tidbit

slug: free-docker-disk-space
---

If you are running out of disk space on your docker host, you can free up some space by removing unused docker images, containers, and volumes.
I am running docker-compose and always watch the logs, since the service is not high scale yet, so the log
files fill up. This means from time to time I need to free some disk space.

TL;DR Here are some commands to free up disk space:

```bash
