---
layout: layouts/blog/post.njk
title: "Setup HTTPS with letsencrypt for a new server"
date: 2024-05-05
dateCreated: "2024-05-05 13:32 CET"
tags:
    - linux
    - https
    - docker
    - letsencrypt
    - tidbit

slug: setup-https-with-letsencrypt-for-a-new-server
---

The following does NOT need to connect the server, which is another way, where the server provides a secret
via http, and the letsencrypt server checks it. But the drawback of that is that you need to run the webserver
and later reconfigure it, since this is only needed for the initial cert afaik. So I went the other way.
Will see how the renewal works, but I think it should work.

The way below is the `--standalone` way, where this http config is not needed on your webserver.
It's the shortest and quickest way to get a cert and make you domain https. 

To create the certs:
- on the server: `mkdir -p /etc/letsencrypt/my.domain`
- `docker pull certbot/certbot`
- `docker run -p 80:80 -it --rm -v "/etc/letsencrypt/my.domain:/etc/letsencrypt" certbot/certbot certonly -d my.domain --standalone`
- after this the renewal should work automatically, see the docker-compose.yml file
- good resource to read about letsencrypt, certbot etc. are the following:
  - https://eff-certbot.readthedocs.io/en/latest/using.html#certbot-commands the real docs
  - https://medium.com/rahasak/setup-lets-encrypt-certificate-with-nginx-certbot-and-docker-b13010a12994 a good in depth article,
    just not exactly the way I need it, but a lot of good info
  - and ChatGPT helped me too :)

## In docker-compose.yml

I am using the following setup to renew the certs automatically:

```yaml
  certbot:
    image: certbot/certbot
    container_name: certbot
    volumes:
      - /etc/letsencrypt/my.domain:/etc/letsencrypt
    entrypoint: '/bin/sh -c "trap exit TERM; while :; do certbot renew; sleep 12h & wait $${!}; done;"'
```

and besides that you also will need a webserver that uses this cert.
