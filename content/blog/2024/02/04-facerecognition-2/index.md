---
layout: layouts/blog/post.njk
title: "Face Recognition, Part 2: Face Detection"
date: 2024-02-04
dateCreated: "2024-02-04 20:01 CET"
tags:
  - ai
  - machine learning
  - python
  - face-recognition
---


In [part 1](/blog/2024/02/02-facerecognition-1/) I discovered the three steps that face recognition consists of,
detection, identification and recognition. Now I want to dive a bit deeper into the first step, face detection
by telling my story how I learned (about) it by doing it.



