/**
 * Include this file in a browser for outlining faces on an image.
 * This uses the FaceDetector API described and in
 * - this blog post https://developer.chrome.com/docs/capabilities/shape-detection#facedetector
 * - and in this specification draft https://wicg.github.io/shape-detection-api/#face-detection-api
 */

/**
 * Call this function, to find out if your browser supports face detection.
 */
const canShowFaces = () => {
  return 'FaceDetector' in window;
};

/**
 * Use the DOM FaceDetector API to detect faces in the image `$img`
 * and create an absolute positioned <div> for each face with a 1px red border.
 * 
 * This function expects a reference to an <img> DOM node and a container where to
 * place the face rectangles into, the latter one is recommended to be `position:relative`.
 * This function returns the number of faces found and references to the DOM nodes it created to 
 * outline the faces on the image.
 * 
 * @param $img {Element<HTMLImageElement>}
 * @param $facesContainer {Element<HTMLDivElement>}
 * @return {Promise<{numFaces: number, nodes: Element<HTMLDivElement>[]}>}
 */
const showFaces = async ($img, $facesContainer) => {
  const faceDetector = new FaceDetector({fastMode: false});
  const faces = await faceDetector.detect($img);
  const imgNaturalWidth = $img.naturalWidth;
  const imgNaturalHeight = $img.naturalHeight;
  const w = $img.width;
  const h = $img.height;

  const ratio = Math.min(w / imgNaturalWidth, h / imgNaturalHeight);
  const vGap = $facesContainer.getBoundingClientRect().width - imgNaturalWidth * ratio;
  const hGap = $facesContainer.getBoundingClientRect().height - imgNaturalHeight * ratio;  
  
  const $els = [];
  faces.forEach(face => {
    const {top, left, width, height} = face.boundingBox;
    const $div = document.createElement('div');
    $div.style.position = 'absolute';
    $div.style.top = `${top * ratio + hGap/2}px`;
    $div.style.left = `${left * ratio + vGap/2}px`;
    $div.style.width = `${width * ratio}px`;
    $div.style.height = `${height * ratio}px`;
    $div.style.border = '1px solid white';
    $facesContainer.appendChild($div);
    $els.push($div);
  });
  return {numFaces: faces.length, nodes: $els};
};