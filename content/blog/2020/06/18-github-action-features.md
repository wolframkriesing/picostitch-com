---
layout: layouts/blog/post.njk
title: "Github Action Features"
date: 2020-06-18
dateCreated: "2020-06-18 17:40 CET"
tags:
    - tools
    - github action
    - deployment
    - tidbit

slug: github-action-features
---

While reading about github actions I note down a couple of things here, that
I find worth noting and remembering. Basically I can imagine what it looks like, 
it's a travis with a tighter integration into github, but some details are interesting.
 
Quotes from https://help.github.com/en/actions/configuring-and-managing-workflows/configuring-a-workflow

I see nothing left to wish for on how to start an automated workflow, even cron jobs exist.

> You can configure a workflow to start once:
>
> * An event on GitHub occurs, such as when someone pushes a commit to a repository or when an issue or pull request is created.
> * A scheduled event begins.
> * An external event occurs.

Steps in a job can share information using the filesystem!

> Each job in a workflow executes in a fresh instance of the virtual environment, and steps 
> within a job can share information using the filesystem.

Running multiple jobs on different configurations is built-in, really nice!

> a workflow can run a job for more than one supported version of a language, 
> operating system, or tool. For each configuration, a copy of the job runs and reports a status.

> There are different types of actions you can use in your workflow to suit your project's needs:
>
> * Docker container actions
> * JavaScript actions
>
> For more information, see ["About actions."](https://help.github.com/en/articles/about-actions#types-of-actions)

On that page I finally learn what an action is.

> Actions require a metadata file to define the inputs, outputs and main entrypoint for your action. 
> The metadata filename must be either action.yml or action.yaml. 
> For more information, see ["Metadata syntax for GitHub Actions."](https://help.github.com/en/articles/metadata-syntax-for-github-actions)

And finally reading through some code in the "setup-node" action repo
https://github.com/actions/setup-node
makes me realize that this is not just like travis, it is so much more complicated on first sight.
Maybe also more powerful, but will also occupy more time to fully understand it.
