---
layout: layouts/blog/post.njk
title: "Is it Github \"Actions\" or \"Workflow\"?"
date: 2020-06-18
dateCreated: "2020-06-18 13:24 CET"
tags:
    - tools
    - github
    - tidbit

slug: is-it-github-actions-or-workflow
---

I wanted to deploy to github-pages using the renamed branch "main", but it seems not to be possible with
github (yet) to turn on github-pages with a branch different then "master".
So I thought it might be a good time to try out github actions, maybe this will help?
If not I will have learned something.

## I am Trying it out
I clicked on the "Actions" tab on my [More HTML project page](https://github.com/more-html/components) 
and chose "set up a workflow yourself &rightarrow;".
The confusion did already start for me, I click on "**action**" and the link here says "set up a **workflow**".
What is it action or workflow?

<figure>
    <img src="github-actions-1.gif" alt="I clicked: set up a workflow yourself" width="500" />
    <figcaption>I clicked: set up a workflow yourself</figcaption>
</figure>

This now opens a file editor for a file ".github/**workflows**/main.yml".
Again workflow, not action. I need to read some docs.

<figure>
    <img src="github-actions-2.gif" alt="&quot;basic workflow to help you get started with Actions&quot;" width="500" />
    <figcaption>&quot;basic workflow to help you get started with Actions&quot;</figcaption>
</figure>

Even at the top of this new file it says

> basic **workflow** to help you get started with **Actions**

I really need to clarify the terms for me.
So I click the "Documentation" link on the right side.
There it says "Getting started with a workflow" and later

> For the full GitHub **Actions** documentation on **workflows**, see [...some link...]

It doesn't get clearer yet.
I follow the suggested link. And already the breadcrumb (see image below)
does help me understand it a bit better. There is a hierachy.
Though still the terms are used in a mixed way. But I seem to be close to figuring it out.

<figure>
    <img src="github-actions-3.gif" alt="Both terms &quot;action&quot; and &quot;workflow&quot; are used." width="500" />
    <figcaption>Both terms &quot;action&quot; and &quot;workflow&quot; are used.</figcaption>
</figure>

On this page [Configuring a workflow](https://help.github.com/en/actions/configuring-and-managing-workflows/configuring-a-workflow)
I finally find relief.

> **Workflows** must have at least one **job**, and jobs contain a set of **steps** that perform 
> individual tasks. Steps can run **commands** or use an **action**.
> You can create your own actions or use actions shared by the GitHub community and customize them as needed.
>
> You can configure a workflow to start when a GitHub event occurs, on a schedule, or from an external event.

Alright, now I get it. Now I have a hierarchy and I understand what the terms mean and how they
are connected. Poooh, finally.

## In the End
I realized Github Actions are way more than just a `.travis` file, if I want to entirely understand them.
 
While searching and thinking about how to do that and how much technology and machines are behind this to make it all work,
I thought that all this does not cost me a dime.
This leaves me with the question, why not move all my code slowly over
to [codeberg.org](https://codeberg.org/), pay for it, control it, own it. 

Github costs no money. What is the price I am paying?
