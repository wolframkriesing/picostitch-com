---
layout: layouts/blog/post.njk
title: "HTML Elements not Upgradable to CBEs"
date: 2020-06-01
dateCreated: "2020-06-01 12:54 CET"
tags:
    - web
    - HTML
    - Web Components
    - tidbit

slug: html-elements-not-upgradable-to-cbes
---

[More HTML](https://github.com/more-html/components)
provides a CBE (Customized Built-in Element) for upgrading heading tags such as H1, H2, ...
to render a link-icon beside them, as many know it from github readme files.\
I wanted to upgrade all picostitch sites automatically by just including a `<script>`
which upgrades all headings on the page and adds this functionality.\
**It is not possible to upgrade a parsed and rendered element (like an H1) afterwards.**

There is a very good longer [explanation of that on stackoverflow](https://stackoverflow.com/a/51527693).
In short the conclusion is:

> The is attribute is used only at element creation (at parse time) to initialize the is value and has no effect if 
> changed when the element is already created. In that sense is value is read-only.

## What is More HTML?

This is a small early stage project I started a while ago. It ...

> ... provides several web components that just enhance HTML.
  Some are provided as pure web components ala `<more-somecomponent>`
  and others can be used via customized built-in elements (CBEs [read more on MDN](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements)), 
  such as `<h1 is=more-h1>`
  which enhances an H1 with certain functionality.
  See the [examples](https://more-html.github.io/components/examples/) for some more in depth explanation and examples.

See the [README](https://github.com/more-html/components).

## What is a CBE?

CBE is the abbreviation for "customized built-in element" and it makes up a part of Custom Elements.
[The spec says](https://html.spec.whatwg.org/multipage/custom-elements.html#custom-elements-customized-builtin-example) 
the following about it.

> Customized built-in elements are a distinct kind of custom element, which are defined slightly differently and used 
> very differently compared to autonomous custom elements. They exist to allow reuse of behaviors from the existing 
> elements of HTML, by extending those elements with new custom functionality.

## What does "upgrade" mean?

"Upgrade" in the context of Web Components or Custom Elements has a specific meaning.
[The spec explains](https://html.spec.whatwg.org/multipage/custom-elements.html#custom-elements-upgrades-examples)
that very well too.

> Because element definition can occur at any time, a non-custom element could be created, and then later become a 
> custom element after an appropriate definition is registered. We call this process "upgrading" the element, 
> from a normal element into a custom element.
