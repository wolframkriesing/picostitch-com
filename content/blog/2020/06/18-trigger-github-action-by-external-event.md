---
layout: layouts/blog/post.njk
title: "Trigger Github Action by External Event"
date: 2020-06-18
dateCreated: "2020-06-18 17:33 CET"
tags:
    - tools
    - github
    - deploy
    - automate
    - webmention
    - tidbit

slug: trigger-github-action-by-external-event
---

Lately I thought more often about automating my blog using [webmentions](https://webmention.io/)
and while reading the Github Actions docs, I found the following might come in handy.
So I am parking that info here.

> To trigger a workflow after an external event occurs, you can invoke a repository_dispatch 
> webhook event by calling the "Create a repository dispatch event" REST API endpoint. 
> For more information, see "Create a repository dispatch event" in the GitHub Developer documentation.

The link is https://developer.github.com/v3/repos/#create-a-repository-dispatch-event
