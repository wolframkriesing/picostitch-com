---
layout: layouts/blog/post.njk
title: "WebStorm: \"New File\" Creates Directories and File"
date: 2020-06-01
dateCreated: "2020-06-01 12:22 CET"
tags:
    - tools
    - WebStorm
    - tidbit

slug: webstorm-auto-create-files-and-directories
---

I saw a video that VS Code creates directories and file when
adding a file named like `dir1/dir2/file.js`, I tried it in WebStorm.
It works.

Just do "New File" (<kbd>Command</kbd> + <kbd>n</kbd> for me) and type
the full path and all directories needed get created. Cool.

<figure>
    <img src="webstorm-newfile.gif" alt="'New file' dialog in WebStorm also accepts path names." width="400" />
    <figcaption>'New file' dialog in WebStorm also accepts path names.</figcaption>
</figure>
