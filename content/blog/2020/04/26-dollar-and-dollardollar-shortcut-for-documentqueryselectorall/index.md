---
layout: layouts/blog/post.njk
title: "`$` and `$$` - Shortcut for `document.querySelector[All]()`"
date: 2020-04-26
dateCreated: "2020-04-26 14:55 CET"
tags:
    - tools
    - knowledgebase
    - tidbit

slug: dollar-and-dollardollar-shortcut-for-documentqueryselectorall
---

I still often type `document.querySelector()` or `document.querySelectorAll()`
even though even before this was available in the browser the developer consoles, FireBugs or whatever
they were called had a shorter version available `$` and `$$`. Yep, it works almost exactly the
same, just that one returns a `NodeList` the other an array. Just try it.
It works in all browsers as far as I know, I tried Firefox, Safari, Chrome and Edge.
See the image below for how it works in Firefox.

<figure>
    <img src="dollar-and-dollardollar.gif" alt="no real output" width=300 class="sizeup-onhover-image scale4 origin-left-center" />
    <figcaption>$ and $$ vs document.querySelector[All]()</figcaption>
</figure>
