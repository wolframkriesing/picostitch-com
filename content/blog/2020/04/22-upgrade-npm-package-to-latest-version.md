---
layout: layouts/blog/post.njk
title: "Upgrade npm Package to Latest Version"
date: 2020-04-22
dateCreated: "2020-04-22 16:35 CET"
tags:
    - javascript
    - npm
    - knowledgebase
    - tidbit

slug: upgrade-npm-package-to-latest-version
---

Run `npm install <package>@latest` to update to the latest version of a package, 
no matter the minor, major version, it always goes to the latest.
