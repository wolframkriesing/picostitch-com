---
layout: layouts/blog/post.njk
title: "Semantic HTML: Usage of Headings, Sections"
date: 2020-04-25
dateCreated: "2020-04-25 22:02 CET"
tags:
    - html
    - knowledgebase
    - tidbit

slug: semantic-html-usage-of-headings-sections
---

Wondering how to layout and use `<section>`, `<h1>`, `<h2>`, etc. I came across
the first place one should read on MDN
[Using HTML sections and outlines](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Using_HTML_sections_and_outlines)
also very insightful and more specific is this on the W3C wiki
[HTML/Usage/Headings/Missing](https://www.w3.org/wiki/HTML/Usage/Headings/Missing).
If you have 27 minutes to learn how to use all those semantic tags watch Brian Haferkamp's 
[Semantic Elements and Structure](https://www.youtube.com/watch?v=-dQ2Big9ueg) a very well explained video.
