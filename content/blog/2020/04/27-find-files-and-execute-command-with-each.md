---
layout: layouts/blog/post.njk
title: "Find Files and Execute Command with Each"
date: 2020-04-27
dateCreated: "2020-04-27 00:16 CET"
tags:
    - linux
    - tidbit

slug: find-files-and-execute-command-with-each
---

You need to 
1. Find files and
2. Execute a command with each found file

This is how it works:\
`find . -name '*.jpg' -exec cp '{}' ../_output'{}' \;`

The command finds all `*.jpg`s via `find . -name '*.jpg'`.
Next with `-exec cp` it is told to execute the command `cp` for every file found.
The `{}` is the placeholder for the filename that find returns.
Play around with each part of the command to see the output.
Maybe for learning start with `find . -name '*.jpg' -exec echo "file1: {}" \;`.
You will also find out how important the `\;` at the end is!
