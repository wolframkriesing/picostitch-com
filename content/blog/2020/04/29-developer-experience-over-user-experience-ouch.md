---
layout: layouts/blog/post.njk
title: "Developer Experience over User Experience - Ouch"
date: 2020-04-29
dateCreated: "2020-04-29 17:35 CET"
tags:
    - tools
    - tidbit

slug: developer-experience-over-user-experience-ouch
---

Jeremy Keith nailed it again:

> But as a general principle, I think this works:
>    User experience, even over developer experience.
> Sadly, I think the current state of “modern” web development reverses that principle.

another one

> user has to pay when developers choose to use megabytes of JavaScript

[Read "Principles and priorities" on his blog adactio.com ...](https://adactio.com/journal/16811)
