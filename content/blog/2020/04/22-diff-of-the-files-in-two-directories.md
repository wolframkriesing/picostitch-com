---
layout: layouts/blog/post.njk
title: "Diff of the Files in Two Directories"
date: 2020-04-22
dateCreated: "2020-04-22 16:35 CET"
tags:
    - linux
    - knowledgebase
    - tidbit

slug: diff-of-the-files-in-two-directories
---

`diff --recursive <dir1> <dir2>` to diff the files and their contents in two directories
I just needed to do some golden master tests after updating a dependency
