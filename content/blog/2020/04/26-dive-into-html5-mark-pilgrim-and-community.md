---
layout: layouts/blog/post.njk
title: "Dive into HTML5 - Mark Pilgrim (and community)"
date: 2020-04-26
dateCreated: "2020-04-26 14:55 CET"
tags:
    - html
    - tidbit

slug: dive-into-html5-mark-pilgrim-and-community
---

I read the real book a long time ago, but I just moved this blog to use
proper `<aside>`, `<article>` and alikes today, I had forgotten a lot.
But I remember it being the best HTML(5) in depth I ever saw.
https://diveinto.html5doctor.com/ a must read!
