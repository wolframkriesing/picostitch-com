---
layout: layouts/blog/post.njk
title: "`<details>` HTML element"
date: 2020-04-25
dateCreated: "2020-04-25 22:34 CET"
tags:
    - html
    - knowledgebase
    - tidbit

slug: details-html-element
---

Click below to see the code which creates this kinda dropdown, it is the `<details>` element.
<details>
    <summary>A &lt;details&gt; code example (click here)</summary>
    <pre>
        &lt;details&gt;
            &lt;summary&gt;A &amp;lt;details&amp;gt; code example (click here)&lt;/summary&gt;
            ... too much recursion ... ;)
        &lt;/details&gt;
    </pre>        
</details>

The `<details>` element "creates a disclosure widget in which information is visible only when the widget is 
toggled into an "open" state" [MDN says](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details).
