---
layout: layouts/blog/post.njk
title: "Web Accessibility in Mind"
date: 2020-04-26
dateCreated: "2020-04-26 14:55 CET"
tags:
    - a11y
    - knowledgebase
    - tidbit

slug: web-accessibility-in-mind
---

https://webaim.org/
with lots of interesting [resources](https://webaim.org/resources/)
and stuff to learn.
