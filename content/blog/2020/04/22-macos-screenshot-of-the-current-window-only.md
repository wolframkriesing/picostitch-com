---
layout: layouts/blog/post.njk
title: "MacOS Screenshot of the Current Window Only"
date: 2020-04-22
dateCreated: "2020-04-22 16:35 CET"
tags:
    - macos
    - knowledgebase
    - tidbit

slug: macos-screenshot-of-the-current-window-only
---

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="en" dir="ltr">
    MacOS: Woa, I didn&#39;t know you can take screenshots of windows with transparent backround 
    by doing CMD+Shift+4 and then hitting the space bar. 🤯 
    <a href="https://t.co/WjdV15q4H0">pic.twitter.com/WjdV15q4H0</a></p>
    &mdash; Nikolai Onken (@nonken) 
    <a href="https://twitter.com/nonken/status/1252701488046051328?ref_src=twsrc%5Etfw">April 21, 2020</a>
</blockquote>
