---
layout: layouts/blog/post.njk
title: "HTML Validators"
date: 2020-04-26
dateCreated: "2020-04-26 14:55 CET"
tags:
    - html
    - tidbit

slug: html-validators
---

Various HTML validator I found:
* [validator.nu](https://validator.nu/) seems to be the same used at [validator.w3.org](https://validator.w3.org/).
  Describes itself as ["Validator.nu is validation 2.0"](https://about.validator.nu/).
* [Structured Data Linter, linter.structured-data.org](http://linter.structured-data.org/)
  "The Linter understands the microdata, JSON-LD and RDFa formats according to their latest specifications."
  [they say](http://linter.structured-data.org/about/)
* [yandex's Structured data validator](https://yandex.com/support/webmaster/yandex-indexing/validator.html)
  "it also checks if the markup meets the requirements of Yandex's services".
* [Bing Markup Validator](https://www.bing.com/toolbox/markup-validator) requires a sign-in and than it
  "shows the markup we’ve discovered, including HTML Microdata, Microformats, RDFa, Schema.org, and OpenGraph".    
* [Google's structured data testing tool](https://search.google.com/structured-data/testing-tool)
  verifies the schema.org (structured data) validity, I assume they validate as it is relevant for their search engine.
