---
layout: layouts/blog/post.njk
title: "Than vs. Then"
date: 2020-04-26
dateCreated: "2020-04-26 14:55 CET"
tags:
    - english
    - writing
    - knowledgebase
    - tidbit

slug: than-vs-then
---

I always struggled with it. Actually I looked it up a couple days ago and until today I was under
the assumption "than" was for time and ordering, I had been confused and wrong.

> **Then** is for time or ordering  
> **Than** is for comparing

The long version, very well explained is at https://ell.stackexchange.com/a/80756
