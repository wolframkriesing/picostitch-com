---
layout: layouts/blog/post.njk
title: "GitUp v1.1.3 is out"
date: 2020-04-29
dateCreated: "2020-04-29 22:34 CET"
tags:
    - tools
    - tidbit

slug: gitup-v113-is-out
---

My favourite git tool GitUp, which allows me to do everything with the keyboard and still has a UI has just received an update
and if I had not read the [release notes](https://github.com/git-up/GitUp/releases) I wouldn't have known that now
the "Fix double clicking title not expanding window" landed.
I had already trained my muscle memory and would probably not have realized.

Read your tools' release notes and changelogs, they make you more productive!
Learned it from [JB](https://twitter.com/jbrains).
