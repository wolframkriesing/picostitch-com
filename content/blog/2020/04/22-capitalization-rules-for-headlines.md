---
layout: layouts/blog/post.njk
title: "Capitalization Rules for Headlines"
date: 2020-04-22
dateCreated: "2020-04-22 16:35 CET"
tags:
    - writing
    - english
    - knowledgebase
    - tidbit

slug: capitalization-rules-for-headlines
---

I didn't know that for English ["sources disagree on the details of capitalizing prepositions"](https://en.wikipedia.org/wiki/Capitalization#Titles). I read so often "capitalize all words of four letters or more". What an arbitrary rule is that? Ok, I will try to follow this and capitalizing all "major words", they call them.\
There are a couple (SEO) sites that capitalize your headline  correctly, you'll find them when you need 'em.
