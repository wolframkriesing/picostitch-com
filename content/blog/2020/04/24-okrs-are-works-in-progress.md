---
layout: layouts/blog/post.njk
title: "OKRs are Works in Progress"
date: 2020-04-24
dateCreated: "2020-04-24 12:01 CET"
tags:
    - okr
    - knowledgebase
    - tidbit

slug: okrs-are-works-in-progress
---

I read it multiple times already in [Measure What Matters](https://www.goodreads.com/book/show/39286958-measure-what-matters)
by John Doerr, that OKRs are flexible. I normally underline those things, but now I have to note it somewhere where I can find it again without physically needing the book. On page 54 he writes

> Remember that an OKR can be modified or even scrapped at any point in its cycle. Sometimes the "right" key results 
> surface weeks or months after a goal is put into play.

The TL;DR for me is the sentence after it

> OKRs are inherently works in progress, not commandments chiseled in stone.

**Learning and mastering OKRs takes time and practice.** On page 68 Brett Kopf of Remind says:

> OKRs are basically simple, but you don't master the process off the bat. Early on, we'd be off by 
> miles in our company-level objectives, mostly on the way-too-ambitious side.

On page 75 David from Nuna confirms that:

> You're not going to get the system just right the first time around. It's not going to be perfect
> the second or the third  time, either. But don't get discouraged. Persevere. You need to adapt it to make it your own.
