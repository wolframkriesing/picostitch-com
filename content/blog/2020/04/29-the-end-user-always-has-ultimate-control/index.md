---
layout: layouts/blog/post.njk
title: "The End User Always has Ultimate Control"
date: 2020-04-29
dateCreated: "2020-04-29 17:11 CET"
tags:
    - a11y
    - knowledgebase
    - tidbit

slug: the-end-user-always-has-ultimate-control
---

While hunting for accessibility resources, especially the reasons why designs need to be flexible
I came across this article [Accessible CSS](https://webaim.org/techniques/css/) on [WebAIM.org](https://WebAIM.org).
And their warning sign just needs to be shown around the web even more.

<figure>
    <img src="webaim-quote-accessible-web.gif" alt="user is in control warning"/>
    <figcaption>Warning that the user has ultimate control</figcaption>
</figure>

[Read "Accessible CSS" on WebAIM.org ...](https://webaim.org/techniques/css/)
