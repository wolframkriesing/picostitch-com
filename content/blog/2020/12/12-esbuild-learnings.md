---
layout: layouts/blog/post.njk
title: "esbuild Learnings"
date: 2020-12-12
dateCreated: "2020-12-12 20:58 CET"
tags:
    - JavaScript
    - bundle
    - build
    - tidbit

slug: esbuild-learnings
---

## Minify all *.js Files
Sometimes reducing the file size is the only thing I want.
No bundling needed, and esbuild can do it. Like so:
```
$ esbuild src/*.js --minify --outdir=dist
```
This finds all `*.js` files in the director `src`,
minifies them and puts the result in the `dist` directory.
