---
layout: layouts/blog/post.njk
title: "Start Dockerized Apps on Boot, on Linux"
date: 2020-12-24
dateCreated: "2020-12-24 15:32 CET"
tags:
    - tools
    - linux
    - command line
    - docker
    - tidbit

slug: start-docker-on-boot-on-linux
---

I want to get my **nextcloud setup to run using docker-compose**, and to start with
I need a linux machine with docker running at start up. This is task #1, it looks
like [this blog post](https://blog.sleeplessbeastie.eu/2020/09/11/how-to-start-docker-service-at-system-boot/)
has the answers how to do it.

## The System

```
$ lsb_release -a
No LSB modules are available.
Distributor ID:	LinuxMint
Description:	Linux Mint 19.3 Tricia
Release:	19.3
Codename:	tricia
```

What the heck is `lsb_release`, never heard of it, and I can't imagine what that term means and 
how I could even ever remember it, since the abbreviation does not sound intuitive.

The man page says the following:
> The lsb_release command provides certain LSB (Linux Standard Base) and distribution-specific information.

## Get Latest docker
```
$ apt upgrade docker.io
Reading package lists... Done
Building dependency tree       
Reading state information... Done
docker.io is already the newest version (19.03.6-0ubuntu1~18.04.2).
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
```

I have seen newer docker flying around. Mmmh. Will postpone this for now.

## See Docker Info from the OS level
Not sure this headline is right and makes sense to others. 
It does to me (until I know better).

```
$ service docker status # OR
$ systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2020-12-24 13:27:29 CET; 2h 14min ago
     Docs: https://docs.docker.com
```

The two commands above show the running docker service and some info and details.

## Make Docker Start on Boot

According to the article above it is "just":
```
$ sudo systemctl enable --now docker
```
and you can check that via
```
$ systemctl is-enabled docker
enabled
```
which print `enabled` now. I am not sure if this was not also the case before.
Let me reboot and see. ... Works! Cool.

## Not Just Text Files?
I learned stuff (which I am afraid I will forget too soon again, that's why it's written here).

What I am really wondering about is, that the Linux philosophy is that all things are
stored in pure text files, afaik. Why was this enabling of docker as a service not a simple 
"add this line in file X"? Maybe it was and I just didn't look in the right place under the hood.
Of the entire thing was a bit more complicated than expected and that's why there are 
scripts that do that for me.

Actually I thought back in the years, when I was doing more of Linux stuff by hand, 
that I used to manually just enter which deamons to start and that was it. But my memories might fool me.

## Just Text Files!
And the answer comes, right the moment after I asked the question and I needed to take the next step in my
nextcloud setup.

My next step is to make the nextcloud, which is running in a docker-compose setup start every time 
the machine boots. So even after a power outage, or whatever on next boot our private cloud will be online again.
No need to have me around all the time.

This [stackoverflow post](https://stackoverflow.com/questions/30449313/how-do-i-make-a-docker-container-start-automatically-on-system-boot/39493500#39493500)
explains exactly how to do it. And it is very simple, I am just listing the three step below, refer to the
so post to get all the details:
* Create a file `> /etc/systemd/system/nextcloud.service`
* Fill in the info, including how to start the service and finally
* `sudo systemctl enable nextcloud`. 

Applying my learnings from above, I think I can cehck if it worked.
```
$ systemctl is-enabled nextcloud.service
enabled
```
And reboot again. My nextcloud is up and running on machine (re)boot.
Yeah.

Here is my service file:
```
$ cat /etc/systemd/system/nextcloud.service
[Unit]
Description=nextcloud
Requires=docker.service
After=docker.service

[Service]
Restart=always
ExecStart=/usr/bin/docker-compose -f /nextcloud/docker-compose.yml up -d
ExecStop=/usr/bin/docker-compose -f /nextcloud/docker-compose.yml down

[Install]
WantedBy=default.target
```

And here is the proof that it all works, really:
```
$ systemctl status nextcloud
● nextcloud.service - nextcloud
   Loaded: loaded (/etc/systemd/system/nextcloud.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2020-12-24 16:54:10 CET; 7s ago
```

## Update Docker Images with docker-compose 
I had built my nextcloud setup about a year ago, a simple `docker-compose build` did not update any of the 
docker images, but I am sure there have been changes to any of the layers, like the DB, the app, the nginx proxy,
etc. But why did it not update?  I tried 
```
$ docker-compose build db
db uses an image, skipping
$ docker-compose build --no-cache app
app uses an image, skipping
```
:(  
None works.

Because the right one is:
```
$ docker-compose pull
Pulling db (mariadb:latest)...
latest: Pulling from library/mariadb
...
```

Oh, and don't just update the nextcloud image and bump multiple major versions, it can't handle it.
Do one major version at a time! #justSaying
If you try `php occ upgrade` it will let you know, but it was already to late for me by then.
