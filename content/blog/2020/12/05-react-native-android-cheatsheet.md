---
layout: layouts/blog/post.njk
title: "React Native Android Cheatsheet"
date: 2020-12-05
dateCreated: "2020-12-05 14:28 CET"
tags:
    - React Native
    - Android
    - tidbit

slug: react-native-android-cheatsheet
---

This is a collection of things I sometimes need when working with React Native and my external Android device.

## List Connected Devices
To see all connected devices run:
`adb devices`.
Unfortunately the names listed are not always helpful. I always plug/unplug the device and figure out which one is which name.
Better ideas? I am interested.

## Open the Developer Menu in the App from the Shell
From the command line in the shell to open the developer menu run this command

`adb shell input keyevent KEYCODE_MENU`

## Reload App from Shell
`adb shell input text "RR"`

## Reconnect Device
Sometimes when I put my computer to sleep, or alike and it disconnects from the device, run the following to restart
the connection (rebuilding the android app works too, but is way slower):
```
$ # In this order!
$ adb reverse tcp:8081 tcp:8081
$
$ react-native start
$ # OR
$ adb shell input text "RR"
$ # OR
$ # <press "r", if you are in the shell where `react-native start` is still running>
```
Unfortunately I also have to restart the app on the device, will need to figure out how to prevent this.

More hints in [this thread on stackoverflow](https://stackoverflow.com/questions/44170991/reload-a-react-native-app-on-an-android-device-manually-via-command-line).

## See Logs
```
$ adb logcat
```

## See Also
* [adb Learnings](/blog/2020/11/25-adb-learnings/) - My findings about how "to use my real android device to preview stuff, not the emulator anymore"
* [Start the Right Android Emulator](/blog/2020/10/22-start-the-right-android-emulator/) - If you have multiple devices installed in the Android Studio, and you need to choose a certain one.
