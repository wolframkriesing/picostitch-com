---
layout: layouts/blog/post.njk
title: "React Native Picker Repo Moved (Again)"
date: 2020-11-30
dateCreated: "2020-11-30 18:05 CET"
tags:
    - React Native
    - mobile development
    - tidbit

slug: react-native-picker-moved-again
---

The [React Native picker](https://github.com/react-native-picker/picker) is a module I had a couple of 
[touchpoints](https://github.com/facebook/react-native/issues/7817#issuecomment-264837382)
with. Even before it had moved repositories. Now it moved again.

## Some History

Back **in 2016** the picker was shipped with [React Native](https://github.com/facebook/react-native).

When I started working with React Native again in September **2020** it had moved out of React Native into 
community repo, judging by the repo name: github.com/react-native-community/picker.

Looking at the repo **today** (while still trying to learn how to style the component on android)
I learned that the repo [had moved again](https://github.com/react-native-picker/picker/pull/155),
this time to [github.com/react-native-picker/picker](https://github.com/react-native-picker/picker).

Reading the commit linked above I learned that the "react-native-community" org seems not to host repos anymore,
which makes sense I think, otherwise everyone would want to be there which makes it kinda like another internet :).
I am just not sure if this means that the component will be maintained more intensively than it was the case in the last years.
Actually it sounds like a good open source project to help with, I felt the pain for a long time and if you browse the issues
I feel that most of them are about styling the picker (on Android).
Let's see if I get my android things solved and lets see then.
