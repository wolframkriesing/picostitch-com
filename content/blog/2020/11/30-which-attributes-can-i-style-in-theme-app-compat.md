---
layout: layouts/blog/post.njk
title: "Which Attributes can I style in `Theme.AppCompat`"
date: 2020-11-30
dateCreated: "2020-11-30 16:52 CET"
tags:
    - React Native
    - Android
    - styling
    - tidbit

slug: which-attributes-can-i-style-in-theme-app-compat
---

https://developer.android.com/guide/topics/ui/look-and-feel/themes?hl=en#CustomizeTheme

> The Android Support Library also provides other attributes you can use to customize your theme extended from 
> Theme.AppCompat (such as the colorPrimary attribute shown above). These are best viewed in [the library's attrs.xml file](https://chromium.googlesource.com/android_tools/+/HEAD/sdk/extras/android/support/v7/appcompat/res/values/attrs.xml)

on the same page

> Note: Attribute names from the support library do not use the `android:` prefix. That's used only for attributes from the Android framework.

The avilable themes

https://chromium.googlesource.com/android_tools/+/HEAD/sdk/extras/android/support/v7/appcompat/res/values/themes.xml
