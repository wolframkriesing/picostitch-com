---
layout: layouts/blog/post.njk
title: "Hacking Android Styles"
date: 2020-11-30
dateCreated: "2020-11-30 16:28 CET"
tags:
    - React Native
    - Android
    - styling
    - mobile
    - knowledgebase
    - tidbit

slug: hacking-android-styles
---

I never did android development, only the React Native side of things.
Now while digging deeper, trying to figure out how to style a React Native picker on android (because the default is really ugly)
I am reading into [android docs](https://developer.android.com/). I found out how to break my app's rendering with one line of XML. Not true, five lines of XML.

In "android/app/src/main/res/values/style.xml" I can style theme my app.
By simply overriding the style `android:textViewStyle` like you see below
I can change all `<Text>` components used in my app that do not override any of the attributes you see below.

```
<resources>
    <!-- Base application theme. -->
    <style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">
        <!-- Customize your theme here. -->
        <item name="android:textViewStyle">@style/TextView</item>
    </style>

    <style name="TextView" parent="AppTheme">
        <item name="android:textColor">#ff0000</item>
        <item name="android:padding">20dp</item>
        <item name="android:gravity">center</item>
    </style>
```

I have overridden `textColor`, `padding` and `gravity` (which is kinda like `justifyContent` in flexbox-speak).
And boom I had a couple of red texts that became centered in my app. I guess that also means I did not yet 
properly style all elements.
