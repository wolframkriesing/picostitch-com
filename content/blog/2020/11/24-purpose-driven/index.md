---
layout: layouts/blog/post.njk
title: "Purpose Driven"
date: 2020-11-24
dateCreated: "2020-11-24 23:05 CET"
tags:
    - purpose
    - company
    - founders
    - sustainable
    - tidbit

slug: purpose-driven
---

> The purpose is the core of the company, not the profit.

<figure>
    <img src="purpose-driven.jpeg" alt="Documentary on arte.tv" height="300" class="sizeup-onhover-image scale2 origin-left-center" />
    <figcaption>Documentary on arte.tv</figcaption>
</figure>

A great documentary about purpose driven companies. Companies that work for the "why" not the "how much".
The interesting part, that is being touched on just slightly, is where the documentary tries to show how
little support our government, laws and types of companies that exist have for this "new model".

Companies like https://www.ecosia.org and https://www.wildplastic.com that are companies that
own themselves, they kinda pulled the plug from capitalism and their money is not what the companies
and the employees work for but the purpose of making the world a bit better.
Basically bringing back the "social" into the social market economy that we are propogating that we would live in,
but that is quite polluted by a lot of greed.
