---
layout: layouts/blog/post.njk
title: "`Theme.AppCompat` is supplied by the Android Support Library"
date: 2020-11-30
dateCreated: "2020-11-30 16:25 CET"
tags:
    - Android
    - tools
    - React Native
    - react
    - mobile development
    - knowledgebase
    - tidbit

slug: theme-app-compat-is-supplied-by-the-android-support-library
---

When I started digging into the `styles.xml` file of our React Native app I found XML like this:

```
<resources>
    <style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">
        <item name="android:textColor">#000</item>
```

I understand that the `parent` attribute there means inherit the styles from the theme
"Theme.AppCompat.Light.NoActionBar", I was searching the web where this theme was defined, until 
I found it while reading [the android docs](https://developer.android.com/guide/topics/ui/look-and-feel/themes?hl=en)
there I found the answer:

> a theme such as `Theme.AppCompat` that's supplied by the Android Support Library

So it ships with android and since this is not open source (I think) I didn't find it.
At least I know now where it comes from ;).
I also found the answer to [what the Android Support Library is](https://developer.android.com/topic/libraries/support-library/).
