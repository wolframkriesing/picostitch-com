---
layout: layouts/blog/post.njk
title: "The History of the Web"
date: 2020-05-09
dateCreated: "2020-05-09 13:26 CET"
tags:
    - web
    - knowledgebase
    - tidbit

slug: the-history-of-the-web
---

In October 17, 1990 IMDb started as a unix script.\
December 25, 1990 Tim Berners-Lee releases WorldWideWeb (later Nexus) on Christmas day, the first ever browser for the web.\
August 6, 1991 Tim Berners-Lee responding to a thread on the alt.hypertext Usenet newsgroup, publicly announces the World Wide Web project for the first time.
[The History of the Web](https://thehistoryoftheweb.com/timeline/) has so interesting stuff. Very worth a read. A great site to spend a lot of time on.
