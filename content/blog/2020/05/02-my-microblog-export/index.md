---
layout: layouts/blog/post.njk
title: "My micro.blog Export"
date: 2020-05-02
dateCreated: "2020-05-02 15:23 CET"
tags:
    - indieweb
    - tidbit

slug: my-microblog-export
---


I just [exported the small number of posts as JSON (12 KB)](./my-microblog-export.json) I had on micro.blog.
