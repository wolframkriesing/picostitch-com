---
layout: layouts/blog/post.njk
title: "TypeScript for JavaScript"
date: 2020-05-14
dateCreated: "2020-05-14 22:19 CET"
tags:
    - JavaScript
    - TypeScript
    - ts4js
    - tidbit

slug: typescript-for-javascript
---

Looking around for resource for JavaScript developers that get into TypeScript I found this
[TypeScript for JavaScript Programmers](https://www.typescriptlang.org/v2/docs/handbook/typescript-in-5-minutes.html)
in the TypeScript handbook.

The quote that sums up TypeScript quite well:

> TypeScript sits as a layer on-top of JavaScript, offering the features of JavaScript and then adds its 
> own layer on top of that. This layer is the TypeScript type system.
