---
layout: layouts/blog/post.njk
title: "Make `arr[0]` Fail Safe"
date: 2020-05-03
dateCreated: "2020-05-03 18:07 CET"
tags:
    - JavaScript
    - knowledgebase
    - tidbit

slug: make-arr0-fail-safe
---

I was just filtering a list `list.filter(someCondition)`
and I only wanted the first element **in case there is one**.
Doing `list.filter(someCondition)[0]` fails when the filtered list
is empty. So I am using `slice(0, 1)`, which returns the first element
if there is one, an empty list otherwise.
Now I do `list.filter(someCondition).slice(0, 1)` and never `list.filter(someCondition)[0]`.
Such small things. 

Here one gets reminded again where functions shine, instead of syntax.
Just like `Reflect.defineProperty()` which return a bool and not like
`Object.defineProperty()` which throws when it didn't work. Less noisy, more explicit code.
Which gets me into why exception throwing sux, but that's a whole topic of its own.
