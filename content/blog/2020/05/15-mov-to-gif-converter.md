---
layout: layouts/blog/post.njk
title: "mov to gif converter"
date: 2020-05-15
dateCreated: "2020-05-15 15:37 CET"
tags:
    - tools
    - tidbit

slug: mov-to-gif-converter
---

Since QuickTime on Mac is great for short screen recordings, but the web can handle animated
gifs better, I need https://cloudconvert.com/mov-to-gif it's a simple and awesome service!\
**Pricing**: "absolutely free for up to 25 conversions per day", totally cool. Thanks!
