---
layout: layouts/blog/post.njk
title: "Learned From \"Don't Make Me Think\""
date: 2020-05-11
dateCreated: "2020-05-11 19:09 CET"
tags:
    - web
    - design
    - UX
    - knowledgebase
    - tidbit

slug: learned-from-dont-make-me-think
---

The things I took away for me until now, while I am still reading [the book](https://www.goodreads.com/book/show/18197267-don-t-make-me-think-revisited):
* Use more headlines "even if the paragraph is just one sentence".
* Move the content closer to the according headline (I [did that yesterday](https://github.com/wolframkriesing/site-stitcher/commit/80bfda03f6ab445618ff50fc4e5d9436cefe7971), made a HUGE diff).
* Make sure the different headlines are easily distinguishable (see [my commit](https://github.com/wolframkriesing/site-stitcher/commit/63fdd25f399398ab2cee11ba0a4f1cd477a5e04d).
* Add a breadcrumb, so users of your page know where they are.
* Remove ~~useless~~ words (I like that one).

<a href="https://www.goodreads.com/book/show/18197267-don-t-make-me-think-revisited">
    <figure>
        <img src="dontmakemethink.jpeg" alt="Dont Make me Think - by Steve Krug" />
        <figcaption>Dont Make me Think, by Steve Krug</figcaption>
    </figure>
</a>

I learned about this book from Uku, [as I wrote in "A Developer Designs"](/blog/2020/04/16-a-developer-designs/).
