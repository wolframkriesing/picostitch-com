---
layout: layouts/blog/post.njk
title: "Disclosure Widgets - by Adrian Roselli"
date: 2020-05-20
dateCreated: "2020-05-20 15:42 CET"
tags:
    - a11y
    - web
    - browser
    - link
    - tidbit

slug: disclosure-widgets-by-adrian-roselli
---

A disclosure widget is a simple control whose sole purpose is to hide or show stuff.
See his article for very in depth content on how to use `<details>` and `<summary>` tags. 
https://adrianroselli.com/2020/05/disclosure-widgets.html
