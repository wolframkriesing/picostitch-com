---
layout: layouts/blog/post.njk
title: "CSS Reset"
date: 2020-05-11
dateCreated: "2020-05-11 22:36 CET"
tags:
    - web
    - CSS
    - knowledgebase
    - tidbit

slug: css-reset
---

First search result for "CSS reset" is by the mighty Eric Meyer
https://meyerweb.com/eric/tools/css/reset/ from quite some time ago.
And looking up the source of his site I see the reset is not really included, also no modified version
as far as I saw it.
Next I came across https://cssreset.com/what-is-a-css-reset/ which nicely explains the whys
and also states the cons. Unfortunately the latter article is missing a date, so I can't say how old 
this knowledge is. That's why I was so very keen on having all items properly dated on my blog.

Since it had been quiet around CSS reset lately in my bubble, I needed to refresh my knowledge a bit.
I take away:

> However, there are multiple benefits of this technique that outweigh any drawbacks, not least the 
> more logical development progression that it afford: paste in your CSS Reset, paste in your base 
> styles (if needed), then define everything else from there. It’s also nice to know that you’ve got 
> your bases covered.

**I will use a CSS Reset only for padding and margin** for the start, to adhere to most of what the
user agent defined.

**[UPDATE]**:\
I found out why the site cssreset.com has no dates, it's a pure SEO site where someone wants
to make money with ads. This decreases my trust in what's written there. Mmmmh.
Reading on https://cssreset.com/privacy/
"SURESWIFT CAPITAL INC., the owner and provider of this Website https://cssreset.com"
I think you can get a picture of the trustworthiness of this site.
