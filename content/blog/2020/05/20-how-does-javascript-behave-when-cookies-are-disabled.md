---
layout: layouts/blog/post.njk
title: "How Does JavaScript Behave when Cookies are Disabled?"
date: 2020-05-20
dateCreated: "2020-05-20 11:14 CET"
tags:
    - web
    - browser
    - javascript
    - tidbit

slug: how-does-javascript-behave-when-cookies-are-disabled
---

With latest privacy awareness I believe cookies are also one sensitive topic. I have no numbers,
but I guess there are more people who have cookies disabled then we believe. If not, I learned something here.

I turned cookies off in Brave, I wanted to see what we need to take care of when cookies are
turned off. My interpretation of that is now, that browsers just transparently handle that.
The only difference is that you always get an empty cookie when they are turned off, no matter how many you set.

See what I tried on the browser console:
```
> // cookies turned OFF (in browser settings)
> document.cookie                // read cookies
""
> document.cookie = 'whatever=1' // set the cookie
> document.cookie                // read cookies
""

> // cookies turned ON (in browser settings)
> document.cookie                // read cookies
"<cookies of the current site, if there are any>"
> document.cookie = 'whatever=1' // set the cookie
> document.cookie                // read cookies
"whatever=1; <cookies of the current site, if there are any>"
```
