---
layout: layouts/blog/post.njk
title: "Tim Berners-Lee about WWW"
date: 2020-10-12
dateCreated: "2020-10-12 16:59 CET"
tags:
    - learning
    - web
    - tidbit

slug: tim-berners-lee-about-www
---

Actually Sir Tim Berners-Lee put the headline "Answers for Young People" above 
[this article](https://www.w3.org/People/Berners-Lee/Kids.html), but this is by far not only for the younger people.
I can highly recommend to read it.

Beside learning about how he invented the WWW, the explanation of URL and learning what happens when you click a link (URL)
I would like to highlight the section where he explains how exciting and useful math is. 
He headlined that with 
[I'm interested in Math -- what exciting stuff is there we don't do at school?](https://www.w3.org/People/Berners-Lee/Kids.html#L325).
I read from it **Education** is a thing ;). If there is nothing you take away, take this.

## Is the Web Good or Bad?
The last question he answers is powerful. Some quotes. No comments needed.

> what is made of the Web is up to us.\
> [...]\
> Let's use the Web to help people understand each other.

Think about it and act like it.
