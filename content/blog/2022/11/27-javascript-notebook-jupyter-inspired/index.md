---
layout: layouts/blog/post.njk
title: "JavaScript Notebook (jupyter inspired)"
date: 2022-11-27
dateCreated: "2022-11-27 13:03 CET"
tags:
    - JavaScript
    - Notebook
    - Jupyter
    - Python
    - Markdown
    - tidbit

slug: javascript-notebook-jupyter-inspired
---

I have been playing around with [@tomayac's](https://twitter.com/tomayac)
awesome [Local Reverse Geocoder](https://github.com/tomayac/local-reverse-geocoder)
project. It basically resolves geo coordinates, latitiude+longitude pairs and
gives you names for it.

While trying it out, I wished for a JS-native [jupyter](https://jupyter.org/)-style notebook.
[I looked around and found some](https://codeberg.org/wolframkriesing/js-notebook#other-alike-projects) 
(inspirations) but nothing that was quite enough, imho.

## Execute JS Snippets in Markdown Files

Using the above reverse-geocoder project, mentioned above, I had to run a number of commands
to get it into a state that I can use it and play around. These were sometimes slow
and I often got it wrong, so I had to retry. I wished for a node-repl that can
(comfortably) replay my commands and ideally store them.

Thinking and playing, trying and swearing I finally knew what I want, I want a node-repl
that I can run inside a markdown, so I can also put down some notes, since I am always good at forgetting
it makes sense to write down things.

The project is still under construction and very early, I am playing around with 
it still, but it works ok-ish for not too complex and not too asynchronous code.
Feel free to try it out at [codeberg.org/wolframkriesing/js-notebook](https://codeberg.org/wolframkriesing/js-notebook).

## js-notebook

This is what it looks like:

<figure>
    <img src="jsntbk.jpeg" alt="JavaScript Notebook Screenshot" height=200 class="sizeup-onhover-image scale2 origin-left-center" />
    <figcaption>JavaScript Notebook Screenshot</figcaption>
</figure>

You can add it to an existing npm project, like so:
```shell
> npm install git+ssh://git@codeberg.org:wolframkriesing/js-notebook.git
