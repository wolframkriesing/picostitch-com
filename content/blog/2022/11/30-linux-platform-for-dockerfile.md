---
layout: layouts/blog/post.njk
title: "Linux Platform for Dockerfile"
date: 2022-11-30
dateCreated: "2022-11-30 19:18 CET"
tags:
    - linux
    - docker
    - amd
    - arm
    - platform
    - tidbit

slug: linux-platform-for-dockerfile
---

I had no idea that docker uses the underlying platform as default also for images that it installs.
My `Dockerfile` that just contained `FROM ubuntu` and I ran some linux binary in there, it threw:
```
qemu-x86_64: Could not open '/lib64/ld-linux-x86-64.so.2
```

Fortunately there is stackoverflow https://stackoverflow.com/a/71611002  
Changing my `Dockerfile` to start with the following, fixes it:
```
FROM --platform=linux/amd64 ubuntu
```

Interesting.
