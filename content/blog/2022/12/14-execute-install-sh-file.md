---
layout: layouts/blog/post.njk
title: "Execute `install.sh` file"
date: 2022-12-14
dateCreated: "2022-12-14 15:58 CET"
tags:
    - linux
    - install
    - bash
    - tidbit

slug: execute-install-sh-file
---

There I have it again, a `install.sh` file on a linux and I don't remember how execute it.
I tried `./install.sh`, error, `. ./install.sh` another error, it also never felt right the way I did it.

I found it: **`bash install.sh`**. That's how it's done.
