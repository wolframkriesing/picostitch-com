---
layout: layouts/blog/post.njk
title: "Verbose docker build"
date: 2022-12-15
dateCreated: "2022-12-15 17:35 CET"
tags:
    - docker
    - tidbit

slug: verbose-docker-build
---

The container build process feels a bit not that transparent, I feel.
But using:
`docker-compose build --progress=plain`
it becomes a bit more transparent.

When running the above, I saw more info about the command, that was run, even
if it is just that it is using a cached result.
```shell
#10 [ 3/13] RUN apt-get update && apt-get install -y build-essential
#10 CACHED

#11 [ 8/13] RUN df -ha
#11 CACHED
```
