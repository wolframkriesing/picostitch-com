---
layout: layouts/blog/post.njk
title: "Learning node-RED"
date: 2022-12-04
dateCreated: "2022-12-04 12:24 CET"
tags:
    - node-red
    - low code
    - no code
    - nodejs
    - JavaScript
    - tidbit

slug: learning-node-red
---

I changed my job inside Sono Motors a bit, I need to learn [node-RED](https://nodered.org/) now.
Node-RED is a low-code, drag-n-drop UI for building execution flows, often used in IOT, but basically can be
used to build any kind of (JavaScript) program. One can install add-ons (nodejs modules) that 
offer any kind of helper, interfaces, connectors, UI, and so on. You can build anything with it.

## Learning by Video

I remember having heard of it before, years ago, but I never paid attention.
Glad I found a good set of short videos to learn how to use node-RED.

<iframe width="427" height="240" name="video" src="//www.youtube.com/embed/3AR432bguOY" frameborder="0" allowfullscreen></iframe>

## My Opinion on the Videos

I am not sure the guy ever said his name, but he does a good job of explaining node-RED.
He is explaining it with few, slow words, but it doesn't need more. Put him on 2x speed, if you feel it's too slow 🏃‍.

The most useful thing for me in these videos was to understand 

## The node-RED Docs

The docs on https://nodered.org/docs/ are ok. I have to say, they somehow feel not like what I was looking
for, but that might be due to the fact that I started out with understanding how to set up a node-RED instance
using docker and persisting its state, so it can also be stored in a git repo, so multiple people can work
with it.

I realized after a while that node-RED is mostly used or promoted to be used with IOT stuff, and many of the
questions and answers feel to be about home automation, even on stackoverflow and the various forums. That was
probably why I didn't find the answer on the questions I asked. 

## Maybe ask ChatGPT?

Writing that, I thought of asking [chatGPT](https://chat.openai.com/chat),
where I have seen great results lately. Let's try. Well, ok-ish, the answer left too many open questions.

Read more about setting up a [Node-RED Setup with Docker and Versioning](../04-node-red-setup-with-docker-and-versioning/).
