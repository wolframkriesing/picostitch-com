---
layout: layouts/blog/post.njk
title: "Docker and Time"
date: 2022-12-08
dateCreated: "2022-12-08 09:32 CET"
tags:
    - docker
    - time
    - timezone
    - tidbit

slug: docker-and-time
---

Inside your docker container you have a wrong time? Then it mostly depends on your timezone.
Mostly it is solved by passing in the timezone from your host into the container, e.g. in the `docker-compose.yml`,
by setting `TZ` environment variable directly, like so.
```dockerfile
    environment:
      - TZ=Europe/Berlin
```
Depending on the base image you are using you might need to add this to the `Dockerfile`, so that the time(zone) tools are installed 
```
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata
```
If your docker image does not come with timezone tools, you might need the above.
The nodejs image for example seems to have it included.

I found the info about [this line for the `Dockerfile` on stackoverflow](https://stackoverflow.com/a/44333806),
and general info in [How to Handle Timezones in Docker Containers](https://www.howtogeek.com/devops/how-to-handle-timezones-in-docker-containers/).
