---
layout: layouts/blog/post.njk
title: "Docker on MacOS (without Docker Desktop)"
date: 2022-12-12
dateCreated: "2022-12-12 21:20 CET"
tags:
    - docker
    - colima
    - mac os
    - tidbit
previewImage: colima.png
slug: docker-on-macos-without-docker-desktop
---

I learned just yesterday that Docker Inc. is charging for their Docker Desktop client.
Only when certain conditions apply, but still. I am not at all against charging for good products, but
now I also understand why the docker app and website wanted me to log in lately.
Good time to get rid of it and use the less annoying tools, 
**I found [colima](https://github.com/abiosoft/colima) as my new docker runtime**, simple, easy, not in my way.

## Uninstalling Docker Desktop

Actually uninstalling docker desktop is not a big deal. Even inside the app you have an "uninstall" button, that's nice.
But then there is no docker deamon running anymore, and I always got the following error message:
```
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
```
Of course, I asked [ChatGPT](https://chat.openai.com/chat) when old-style search engines results didn't help. But
also there I did not really find things that solved my problem, I too often got the answer to run
`sudo launchctl load /Library/LaunchDaemons/com.docker.vmnetd.plist` that did not make a difference.
I even restarted my machine multiple times, which I rarely do. I tried [minikube](https://minikube.sigs.k8s.io/docs/),
I even followed the advice to install docker via `brew install --cask docker`, which just installs
Docker Desktop again. Finally, it's not that easy to do it right if you are not that at home
in the docker world, or I should better say container world.

## The Solution

If you want to run docker without Docker Desktop on MacOS [colima](https://github.com/abiosoft/colima) is the
best replacement I found.
In a [reddit thread](https://www.reddit.com/r/docker/comments/r0r86r/yes_you_can_use_docker_without_docker_desktop_on/)
I came across [lima](https://github.com/lima-vm/lima) for the first time, I didn't understand what to do with it
after skimming the project site, but it sounded like what I need. And then I saw it:
**Colima: Docker on macOS with minimal setup**. That is what I was looking for. And it was as easy as it should be:
```shell
> bew install colima
> colima start
```
All this took a while to install, but that also somehow made me understand that there is some OS stuff to do
in order to run docker, sounds logical, so this must be good.

A `docker version` showed proper results, no docker deamon error anymore, and my `docker-compose up` command
of my latest project also worked, the world looks good again. 🎉

## German Podcast about the Container World

If you are interested in a podcast that explains docker, it's history and the layers below I can recommend
th, unfortunately  🇩🇪-only, Engineering Kiosk Podcast and their 
[episode(s) on Docker](https://engineeringkiosk.dev/podcast/episode/46-welches-problem-l%C3%B6st-docker/).
