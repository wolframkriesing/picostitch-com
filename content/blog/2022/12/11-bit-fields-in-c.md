---
layout: layouts/blog/post.njk
title: "Bit-Fields in C"
date: 2022-12-11
dateCreated: "2022-12-11 19:50 CET"
tags:
    - C
    - programming language
    - types
    - tidbit

slug: bit-fields-in-c
---

I had not yet heard of a type (not sure if it's a type?) in C called bit-fields, or at least I can't remember anymore.
Let's look at one:
```c 
struct {
   unsigned int isLoggedIn : 1;
   unsigned int isCustomer : 1;
   unsigned int isAbove18 : 1;
} person;
```
each member in this struct takes up 1 bit (the `: 1` indicated that).
This allows for optimizing the memory needed.

As far as I read the structure will use 4 bytes, and can contain up to
32 one-bit fields, above it will use any multiple of it.

I learned this [one tutorialspoint "C - Bit Fields"](https://www.tutorialspoint.com/cprogramming/c_bit_fields.htm).
