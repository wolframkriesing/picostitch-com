---
layout: layouts/blog/post.njk
title: "Node-RED Setup with Docker and Versioning"
date: 2022-12-04
dateCreated: "2022-12-04 12:50 CET"
tags:
    - node-red
    - git
    - version control
    - collaboration
    - tidbit
previewImage: nodered-preview.jpeg
slug: node-red-setup-with-docker-and-versioning
---

As [described before](../04-learning-node-red/), I am learning [node-RED](https://nodered.org/). The first thing though that I always want to ensure is
that a project can be cloned (from git) and run with (ideally) just one click.  
For this the defacto standard nowadays is [docker](https://docs.docker.com/), which allows to set up an environment that can
be reproduced on any system and therefore also allows to reduce the cost of setup, sync, and it improves
easier collaboration.

## Docker Setup for node-RED

The [node-RED docs](https://nodered.org/docs/) do describe how to use docker to set up node-RED, but somehow they
just cover the most basic use cases, I feel. And e.g. not how set up a project that can
be versioned and shared among people using version control!

Why version controlling and not installing it just once for the team in the cloud and
then let them all use it? Well, esp. for IOT it makes a lot of sense, when you are
building or developing your device, which resides on your desk, that you can connect
straight to it and doing that over the cloud is just more hassle than just running node-RED locally. 

## Set up Node-RED for Collaboration using git

I created [a repository "node-red-setup"](https://codeberg.org/wolframkriesing/node-red-setup/src/branch/main/)
where I built a way to set up a repo so that it can contain node-RED configs and
changes, is easy to put under version control and good to collaborate.

## Goals of this Project

For enabling the use of node-RED in a collaborative way it needs to enable:
1) low-effort getting started, easy project setup, checkout, run and start working
2) storing changes in a git repo
3) allow seeing changes in git (at least as good as node-RED allows)
4) persist and provide palettes (modules) that got installed in node-RED for all users easily
5) store the right and relevant changes in the git repo (e.g. not a huge load of node-modules)

One core **use case might be**: one (person/team) sets up a node-RED project, creates first flows
and wants to provide this to their colleagues for collaboration, and puts it into a git repository.
From now on it must be easy for everyone to get started in no time and make changes through simple
git workflows, without focusing too much on the node-RED internals, which is something if they are
interested will learn over time. But to get started and collaborate this should not be needed.

## How to use this Project?

This project can be seen like a template setup, if the goals (see above) are those you share
or want to achieve too, then you can work with this project as you like:
* clone/fork this project and keep working in your repo 
* copy+paste all the files, and commit them to your version control and keep working from there
* extract the learnings, and roll your own way of a node-RED setup
* read through [all the commits](https://codeberg.org/wolframkriesing/node-red-setup/commits/branch/main), 
  that led to the current state of this project to learn why it is the way it is
* or any other way you like

## Run node-RED

In order to run this node-RED project, [make sure to have docker installed](https://docs.docker.com/).
The `docker-compose` command we are using here comes with docker.   
And then run this command from your command line:
```shell
> docker-compose up
```
Open your browser at [localhost:1880](http://localhost:1880) and you should get greeted with the node-RED
getting-started dialog.

Click `Ctrl`+`C` to stop the container. All data will be stored in the directory `data` in this project.
Changes done in the node-RED UI are now persisted and will survive docker container restarts, aka multiple runs of `docker-compose up`.

## My Example

Since I have not that much experience with node-RED yet, I touched it for the first time three days ago.
So I thought I build a small example. I built a simple dashboard that:
* loads all commits from the codeberg API
* I format the data a bit
* using a slider one can choose a commit and see details

I used two palettes (modules) like the default dashboard module and a markdown converter. See the result below.
Please note this is just a toy project, and I know it has not design appeal ;).

<figure>
    <img src="commit-browser.jpeg" alt="Commit browser flow" height=200 class="sizeup-onhover-image scale4 origin-left-center" />
    <figcaption>Commit browser flow</figcaption>
</figure>
<figure>
    <img src="commit-browser-result.jpeg" alt="Commit browser result" height=200 class="sizeup-onhover-image scale4 origin-left-center" />
    <figcaption>Commit browser result</figcaption>
</figure>
