---
layout: layouts/blog/post.njk
title: "Mocha's Magic `done` Parameter"
date: 2019-12-02
dateCreated: "2019-12-02 22:30 CET"
tags:
    - javascript
    - nodejs
    - mocha
    - testing
    - knowledgebase
    - tidbit

slug: mochas-magic-done-parameter
---

Why a #mocha #test times out, when I write it like this: `it('...', _ => {});` but it does NOT time out, 
when I write: `it('...', () => {});`? Exactly, because the `_` is the magic `done`, that one needs to call.

Originally posted at http://wolframkriesing.micro.blog/2019/12/02/why-a-mocha.html
