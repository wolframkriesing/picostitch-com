---
layout: layouts/blog/post.njk
title: "Tests using `process.cpuUsage` and `process.memoryUsage`"
date: 2019-12-26
dateCreated: "2019-12-26 13:16 CET"
tags:
    - javascript
    - nodejs
    - learning
    - testing
    - knowledgebase
    - tidbit

slug: tests-using-processcpuusage-and-processmemoryusage
---

Learning stuff about nodejs (or v8) while writing tests that ensure runtime behaviour, 
using process.cpuUsage and process.memoryUsage. Curious how brittle those tests become over time. 
Glad the app ALWAYS runs in the same docker container (dev and prod).

Originally posted at https://wolframkriesing.micro.blog/2019/12/26/learning-stuff-about.html
