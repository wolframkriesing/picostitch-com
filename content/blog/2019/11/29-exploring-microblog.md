---
layout: layouts/blog/post.njk
title: "Exploring micro.blog"
date: 2019-11-29
dateCreated: "2019-11-29 20:23 CET"
tags:
    - indieweb
    - tidbit

slug: exploring-microblog
---

The tries I had with "tweeting" from [micro.blog](https://micro.blog) and how it lands on twitter and 
also how I see and can act on replies is still too basic. I know I am twitter spoiled, 
but it's currently my source of information. Should I build my own #indieweb twitter frontend? mmmh

Originally posted at http://wolframkriesing.micro.blog/2019/11/29/the-tries-i.html
