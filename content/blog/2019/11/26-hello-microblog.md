---
layout: layouts/blog/post.njk
title: "Hello micro.blog"
date: 2019-11-26
dateCreated: "2019-11-26 09:31 CET"
tags:
    - indieweb
    - tidbit

slug: hello-microblog
---

hello micro.blog :) learned about you at Berlin's #IndieWebCamp ... so glad I will finally really own my tweets

Originally posted at http://wolframkriesing.micro.blog/2019/11/26/hello-microblog-learned.html
