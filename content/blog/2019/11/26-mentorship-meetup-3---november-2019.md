---
layout: layouts/blog/post.njk
title: "Mentorship Meetup #3 - November 2019"
date: 2019-11-26
dateCreated: "2019-11-26 15:29 CET"
tags:
    - meetup
    - mentoring
    - learning
    - teaching
    - sharing
    - caring
    - tidbit

slug: mentorship-meetup-3---november-2019
---

Let us talk about mentorship.

This is just "Stammtisch", no talks, no presentation planned. We just sit together and talk about mentoring, 
mentors and mentees and everything that has to do with it. If you have experience with it, if you are planning on 
doing it or you are just curious feel free to join and share.

Today 19:00 - 21:00 CET Join in person, in Munich, Landsberger Straße 314 at Mayflower Join remotely via https://whereby.com/mentorship-meetup

https://gettogether.community/events/3049/mentorship-meetup/

Originally posted at http://wolframkriesing.micro.blog/2019/11/26/if-i-look.html
