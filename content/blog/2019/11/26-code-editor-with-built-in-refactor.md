---
layout: layouts/blog/post.njk
title: "Code Editor with built-in Refactor"
date: 2019-11-26
dateCreated: "2019-11-26 11:21 CET"
tags:
    - tools
    - tidbit

slug: code-editor-with-built-in-refactor
---

what if an code editor had #refactoring as the default when changing variables names, etc. 
and not as a sub-menu item which one has to search for

Originally posted at http://wolframkriesing.micro.blog/2019/11/26/what-if-an.html
