import { walk } from "https://deno.land/std/fs/mod.ts";
import { dirname, join } from "https://deno.land/std/path/mod.ts";

interface Post {
  title: string;
  slug: string;
  dateCreated: string;
  tags: string[];
  content: string[];
  previewImage?: string;
}

function escapeTitle(title: string): string {
  const escaped = title.replace(/"/g, '\\"');
  return `"${escaped}"`;
}

async function processTidbitFile(filePath: string): Promise<Post[]> {
  const content = await Deno.readTextFile(filePath);
  const lines = content.split("\n");
  const posts: Post[] = [];
  let currentPost: Post | null = null;

  for (const line of lines) {
    // New post starts with #
    if (line.startsWith("# ")) {
      if (currentPost) {
        posts.push(adjustImagePaths(currentPost));
      }
      currentPost = {
        title: line.substring(2),
        slug: "",
        dateCreated: "",
        tags: [],
        content: [],
      };
      continue;
    }

    if (!currentPost) continue;

    // Parse metadata
    if (line.startsWith("slug:")) {
      currentPost.slug = line.substring(5).trim();
    } else if (line.startsWith("dateCreated:")) {
      currentPost.dateCreated = line.substring(12).trim();
    } else if (line.startsWith("tags:")) {
      currentPost.tags = line.substring(5)
        .split(",")
        .map((tag) => tag.trim())
        .filter((tag) => tag.length > 0);
    } else if (line.startsWith("previewImage:")) {
      currentPost.previewImage = line.substring(13).trim();
    } else {
      currentPost.content.push(line);
    }
  }

  // Don't forget the last post
  if (currentPost) {
    posts.push(currentPost);
  }

  return posts;
}

function createFrontMatter(post: Post): string {
  return `---
layout: layouts/blog/post.njk
title: ${escapeTitle(post.title)}
date: ${post.dateCreated.split(" ")[0]}
dateCreated: "${post.dateCreated}"
tags:
    ${post.tags.length ? `- ${post.tags.join("\n    - ")}` : ""}
    - tidbit
${post.previewImage ? `previewImage: ${post.previewImage}` : ""}
slug: ${post.slug}
---
`;
}

const imagePattern =
  /<img[^>]+src="([^"]+)"[^>]*>|!\[[^\]]*\]\(([^)]+)\)|previewImage: "([^"]+)"/g;
async function hasImages(post: Post): Promise<boolean> {
  const contentStr = post.content.join("\n");
  return imagePattern.test(contentStr) || !!post.previewImage;
}

async function copyImages(
  post: Post,
  sourceDir: string,
  targetDir: string,
) {
  const contentStr = post.content.join("\n");
  const matches = [...contentStr.matchAll(imagePattern)];

  const imagesToCopy = new Set<string>();
  
  // Add content images
  for (const match of matches) {
    const imagePath = match[1] || match[2];
    if (imagePath && !imagePath.startsWith("http")) {
      imagesToCopy.add(imagePath);
    }
  }
  
  // Add previewImage if exists
  if (post.previewImage && !post.previewImage.startsWith("http")) {
    imagesToCopy.add(post.previewImage);
  }

  for (const imagePath of imagesToCopy) {
    const resolvedSourcePath = join(
      dirname(sourceDir),
      imagePath.replace(/^\.\.\//, ""),
    );
    const fileName = imagePath.split("/").pop();
    const targetPath = join(targetDir, fileName!);

    try {
      await Deno.copyFile(resolvedSourcePath, targetPath);
      console.log(`Copied image: ${resolvedSourcePath} to ${targetPath}`);
    } catch (error) {
      console.warn(`Failed to copy image ${resolvedSourcePath}: ${error}`);
    }
  }
}

async function writePost(post: Post, baseDir: string, sourcePath: string) {
  if (!post.dateCreated || !post.slug) {
    console.warn(`Skipping post with missing required metadata: ${post.title}`);
    return;
  }

  const date = new Date(post.dateCreated.split(" ")[0]);
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0");
  const day = date.getDate().toString().padStart(2, "0");

  const needsDirectory = await hasImages(post);
  const postPath = join(baseDir, String(year), month);
  const finalPath = needsDirectory
    ? join(postPath, `${day}-${post.slug}`)
    : postPath;

  await Deno.mkdir(finalPath, { recursive: true });

  const content = [
    createFrontMatter(post),
    post.content.join("\n").trim(),
    "",
  ].join("\n");

  if (needsDirectory) {
    await copyImages(post, sourcePath, finalPath);
    await Deno.writeTextFile(join(finalPath, "index.md"), content);
  } else {
    await Deno.writeTextFile(
      join(finalPath, `${day}-${post.slug}.md`),
      content,
    );
  }
}

async function main() {
  const tidbitsDir = "content/tidbit";
  const blogOutputDir = "content/blog";

  try {
    for await (
      const entry of walk(tidbitsDir, {
        match: [/index\.md$/],
        skip: [/node_modules/],
      })
    ) {
      if (entry.isFile) {
        console.log(`Processing ${entry.path}`);
        const posts = await processTidbitFile(entry.path);

        for (const post of posts) {
          await writePost(post, blogOutputDir, entry.path);
          console.log(`Created post: ${post.slug}`);
        }
      }
    }
  } catch (error) {
    console.error("Error processing files:", error);
  }
}

function adjustImagePaths(currentPost: Post): Post {
  currentPost.content = currentPost.content.map((line) => {
    if (line.includes('<img src="..')) {
      return line.replace("../", "");
    }
    return line;
  });
  if (currentPost.previewImage && currentPost.previewImage.startsWith("../")) {
    currentPost.previewImage = currentPost.previewImage.replace("../", "");
  }
  return currentPost;
}

if (import.meta.main) {
  main();
}
