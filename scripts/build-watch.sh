#!/usr/bin/env bash

set -o errexit  # fail on simple (non-piped) error
set -o pipefail # also fail on piped commands (e.g. cat myfile.txt | grep timo)
set -o nounset  # fail when accessing unset vars

function build_all {
  local start_time
  local end_time
  local build_time

  start_time=$(gdate +%s%3N)
  if [[ "${1-}" == "--sites-only" ]]; then
    docker-compose run --no-TTY node npm run build:sites
  else
    docker-compose run --no-TTY node npm run build:assets && docker-compose run --no-TTY node npm run build:sites
  fi

  if [ $? -eq 0 ]; then
    end_time=$(gdate +%s%3N)
    build_time=$(echo "scale=2; ($end_time - $start_time) / 1000 " | bc)
    osascript -e "display notification \"Build completed in ${build_time} seconds\" with title \"✅ picostitch.com build\""
  else
    osascript -e 'display notification "Build failed!" with title "❌ picostitch.com build"'
  fi
}

export -f build_all

nodemon --watch src --watch content --watch templates --ext md,js,html,css,json,cjs,mjs --exec "bash -c 'build_all \"$@\"'"
