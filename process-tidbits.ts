import { parse as parseFrontMatter } from "https://deno.land/x/frontmatter/mod.ts";
import { ensureDir } from "https://deno.land/std/fs/ensure_dir.ts";
import { join } from "https://deno.land/std/path/mod.ts";
import { walk } from "https://deno.land/std/fs/walk.ts";

function isValidDate(date: Date): boolean {
  return date instanceof Date && !isNaN(date.getTime());
}

function parseDate(dateStr: string): Date | null {
  const cleanDateStr = dateStr.replace(' CET', '');
  const date = new Date(cleanDateStr);
  return isValidDate(date) ? date : null;
}

function getRedirectUrl(filePath: string): string {
  let url = filePath.replace(/^.+content\/blog\//, '').replace(/\/index\.md$/, '');
  url = url.replace(/\.md$/, '');
  return url;
}

async function processBlogs() {
  const blogDir = join(Deno.cwd(), 'content/blog');
  const tidbitsDir = join(Deno.cwd(), 'content/tidbits');

  for await (const entry of walk(blogDir, { 
    match: [/\.md$/],
    followSymlinks: false 
  })) {
    if (entry.isFile) {
      const content = await Deno.readTextFile(entry.path);
      const { data } = parseFrontMatter(content);
      
      if (data.tags && Array.isArray(data.tags) && data.tags.includes('tidbit')) {
        if (!data.slug) {
          console.error('\x1b[31m%s\x1b[0m', `Error: Missing slug in ${entry.path}`);
          continue;
        }

        const date = parseDate(data.dateCreated || data.date);
        if (!date) {
          console.error('\x1b[31m%s\x1b[0m', `Error: Invalid date in ${entry.path}`);
          continue;
        }

        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        
        const targetDir = join(tidbitsDir, String(year), month, data.slug);
        const redirectContent = `---
redirectUrl: /blog/${getRedirectUrl(entry.path)}/
layout: layouts/301.njk
---
`;

        try {
          await ensureDir(targetDir);
          await Deno.writeTextFile(join(targetDir, 'index.md'), redirectContent);
          console.log(`Created redirect for: ${data.slug} (${year}/${month})`);
        } catch (err) {
          console.error('\x1b[31m%s\x1b[0m', `Error processing ${entry.path}: ${err.message}`);
        }
      }
    }
  }
}

processBlogs().catch(err => {
  console.error('\x1b[31m%s\x1b[0m', `Fatal error: ${err.message}`);
  Deno.exit(1);
});
