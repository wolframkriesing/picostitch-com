import { walk } from "https://deno.land/std/fs/mod.ts";

const FRONT_MATTER_MARKER = '---';

function escapeTitle(title: string): string {
  const escaped = title.replace(/"/g, '\\"');
  return `"${escaped}"`;
}

function formatTags(tagsString: string): string[] {
  return tagsString.split(',').map(tag => tag.trim());
}

function findFirstH1Title(lines: string[]): string {
  for (const line of lines) {
    const trimmed = line.trim();
    if (trimmed.startsWith('# ')) {
      return trimmed.substring(2).trim();
    }
  }
  return '';
}

async function convertHeaders() {
  for await (const entry of walk("content/blog", { 
    includeDirs: false,
    exts: ['.md'],
    skip: [/\.git/]
  })) {
    const content = await Deno.readTextFile(entry.path);
    
    if (content.startsWith(FRONT_MATTER_MARKER)) {
      continue;
    }

    const lines = content.split('\n');
    const headers = new Map<string, string>();
    let headerEndIndex = -1;
    const h1Title = findFirstH1Title(lines);

    for (let i = 0; i < lines.length; i++) {
      const line = lines[i].trim();

      if (line === '' && i > 0 && headerEndIndex === -1) {
        headerEndIndex = i;
      }

      if (headerEndIndex === -1 && line.includes(':')) {
        const [key, ...valueParts] = line.split(':');
        if (valueParts.length) {
          headers.set(key.trim(), valueParts.join(':').trim());
        }
      }
    }

    if (!h1Title) {
      console.warn(`Warning: No H1 title found in ${entry.path}`);
      continue;
    }

    const frontMatter = [
      '---',
      'layout: layouts/blog/post.njk',
      `title: ${escapeTitle(h1Title)}`,
    ];

    if (headers.has('dateCreated')) {
      const dateStr = headers.get('dateCreated')!.split(' ')[0];
      frontMatter.push(`date: ${dateStr}`);
      frontMatter.push(`dateCreated: "${headers.get('dateCreated')}"`);
    }

    if (headers.has('tags')) {
      const tags = formatTags(headers.get('tags')!);
      frontMatter.push('tags:');
      tags.forEach(tag => frontMatter.push(`  - ${tag}`));
      headers.delete('tags');
    }

    for (const [key, value] of headers.entries()) {
      if (key !== 'dateCreated') {
        frontMatter.push(`${key}: ${value}`);
      }
    }

    frontMatter.push('---\n');

    const contentWithoutH1 = lines
      .filter(line => !line.trim().startsWith('# '))
      .slice(headerEndIndex + 1);
    
    const newContent = [
      ...frontMatter,
      ...contentWithoutH1
    ].join('\n');

    await Deno.writeTextFile(entry.path, newContent);
    console.log(`Converted: ${entry.path}`);
  }
}

await convertHeaders();
