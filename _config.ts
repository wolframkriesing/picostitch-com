import lume from "lume/mod.ts";
import nunjucks from "lume/plugins/nunjucks.ts";
import checkUrls from "lume/plugins/check_urls.ts";
import { minify } from "npm:html-minifier-terser";

const site = lume({
  location: new URL("https://picostitch.com"),
  src: "content",
  server: {
    port: 5002,
  },
}, {
  markdown: {
    options: {
      linkify: true,
    },
  },
});

import { toReadableDate, toReadableYearAndMonth } from "./src/_shared/date.js";
site.filter("toReadableDate", toReadableDate);
site.filter("toReadableYearAndMonth", toReadableYearAndMonth);
site.filter("toTransparency", (v) => 100 - (v / 3 || 0));
site.filter(
  "mostRelevantJobsCount",
  (jobs) => jobs.filter((j) => j.isMostRelevant).length,
);

site.use(nunjucks());

site.copy([
  ".css",
  ".ico",
  ".jpg",
  ".jpeg",
  ".png",
  ".gif",
  ".webp",
  ".svg",
  ".json",
  ".js",
  ".avif",
]);

import { slug } from "./src/_shared/slug.js";
import { findRelatedPosts } from "./src/blog-post/related-posts.js";

site.preprocess([".md"], (pages) => {
  // enhance all pages with post data first
  for (const page of pages) {
    page.headline = page.data.title;
  }

  for (const page of pages) {
    page.data.post = {
      dateCreated: page.data.dateCreated ?? page.data.date.toISOString(),
      tags: page.data.tags.map((t) => ({ value: t, slug: slug(t) })),
      relatedPosts: findRelatedPosts(page, pages).map((p) => p.data),
      previewImage: page.data.previewImage,
      previewImageUrl: page.data.url + page.data.previewImage,
    };

    if (!page.data.slug) {
      page.data.slug = slug(String(page.data.title));
    }

    // Get all lines until first empty line
    const abstract = String(page.data.content)
      .split(/\n\n/)[0]; // Split on empty lines and take first part
    page.data.abstract = abstract;

    // if (String(page.headline).includes("parseInt()")) {
    //   console.log(page);
    // }
  }
});

// Minify the HTML, so it is smaller but stays readable, not all on one line.
// I like to look into the HTML from time to time.
site.process([".html"], async (pages) => {
  for (const page of pages) {
    if (typeof page.content === "string") {
      page.content = await minify(page.content, {
        collapseWhitespace: true,
        conservativeCollapse: true,
        preserveLineBreaks: true,
      });
    }
  }
});

site.use(checkUrls());

export default site;
