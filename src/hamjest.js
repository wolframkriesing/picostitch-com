import hamjest from 'hamjest';

export const { assertThat, equalTo, contains, not, hasProperty, hasProperties, endsWith, hasItem, hasItems, startsWith, everyItem, containsString, hasSize, matchesPattern, instanceOf } = hamjest;