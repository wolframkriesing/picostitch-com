import nunjucks from "nunjucks";
import {TEMPLATES_DIRECTORY} from "../config.js";
import {toReadableDate, toReadableYearAndMonth, toWeekday} from "../_shared/date.js";
import {minify} from 'html-minifier-terser';

const nunjucksOptions = {
  autoescape: true,
  throwOnUndefined: true,
  trimBlocks: true,
  lstripBlocks: true,
};

const nunjucksEnv = new nunjucks.Environment(new nunjucks.FileSystemLoader(TEMPLATES_DIRECTORY), nunjucksOptions);
nunjucksEnv.addFilter('toReadableDate', toReadableDate);
nunjucksEnv.addFilter('toReadableYearAndMonth', toReadableYearAndMonth);
nunjucksEnv.addFilter('toWeekday', toWeekday);

const minifyOptions = {
  collapseWhitespace: true,
  conservativeCollapse: true,
  preserveLineBreaks: true,
};

/**
 * @param templateFilename {string}
 * @param data {PlainObject}
 * @return {Promise<string>}
 */
export const renderTemplate = async (templateFilename, data) => {
  return await minify(nunjucksEnv.render(templateFilename, data), minifyOptions);
}

/**
 * @param s {string}
 * @param data {PlainObject}
 * @return {string}
 */
export const renderString_forTesting = (s, data) => {
  return nunjucksEnv.renderString(s, data);
}

