import * as path from 'node:path';

const root = path.resolve('.');
export const CONTENT_DIRECTORY = path.join(root, 'content');
export const TEST_CONTENT_DIRECTORY = path.join(root, 'test-content');
export const BLOG_POSTS_DIRECTORY = path.join(CONTENT_DIRECTORY, 'blog-posts');
export const TEMPLATES_DIRECTORY = path.join(root, 'templates');
export const OUTPUT_DIRECTORY = path.join(root, '_output');
