import {writeOutputFile} from '../_deps/fs.js';
import {renderTemplate} from '../_deps/render-template.js';

/**
 * @typedef {import('../load-tidbit/Tidbit').Tidbit} Tidbit
 * @typedef {import('./render-page').IndexPageProductionDependencies} IndexPageProductionDependencies
 * @typedef {import('./render-page').SinglePageProductionDependencies} SinglePageProductionDependencies
 */

/**
 * @param data {PlainObject}
 * @return {Promise<string>}
 */
const renderIndexPage = async (data) => await renderTemplate('tidbit/index.html', data);
/**
 * @param data {PlainObject}
 * @return {Promise<string>}
 */
const renderSinglePage = async (data) => await renderTemplate('tidbit/page.html', data);
/**
 * @param deps {IndexPageProductionDependencies}
 * @return {function(Tidbit[], PlainObject): Promise<void>}
 */
export const renderAndWriteTidbitsIndexPage = ({writeFile = writeOutputFile, renderPage = renderIndexPage} = {}) => async (tidbits, renderParams) => {
  await writeFile('/tidbits/index.html', await renderPage({...renderParams, tidbits}));
};
/**
 * @param deps {SinglePageProductionDependencies}
 * @return {function(Tidbit[], PlainObject): Promise<void>}
 */
export const renderAndWriteTidbitPages = ({writeFile = writeOutputFile, renderPage = renderSinglePage} = {}) => async (tidbits, renderParams) => {
  const pageWriters = tidbits.map(async t => writeFile(t.url + 'index.html', await renderPage({
    ...renderParams,
    tidbit: t
  })));
  await Promise.all(pageWriters);
};

/**
 * @param data {PlainObject}
 * @return {Promise<string>}
 */
const renderTagPage = async (data) => await renderTemplate('tidbit/tag.html', data);

/**
 * @param deps? {TagPageProductionDependencies}
 * @return {function(ArticlesGroupedByTag[], PlainObject): Promise<void>}
 */
export const renderAndWriteTidbitTagPages = ({writeFile = writeOutputFile, renderPage = renderTagPage} = {}) => async (groups, renderParams) => {
  /**
   * @param tagSlug {string}
   * @return {string}
   */
  const destFilename = tagSlug => `/tidbits/tag/${tagSlug}/index.html`;
  /**
   * @param group {ArticlesGroupedByTag}
   * @return {Promise<void>}
   */
  const writeGroup = async group => writeFile(destFilename(group.tagSlug), await renderPage({
    ...renderParams,
    tag: group.tagSlug,
    articles: group.articles
  }));
  const pageWriters = groups.map(writeGroup);
  await Promise.all(pageWriters);
}

