## <img>.naturalWidth

https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement/naturalWidth 

## HTML: Reset Table Cell Spacing
For a long time I was still using `<table cellspacing=0>`
and I was sure this can be prevented, so I can just use `<table>` and have all cells not be separated.
This is the way to do it in your `reset.css`:
```
table {
    border-spacing: 0;
}
```


## Generate hotels.json  
I made the sheet public so I can export it to JSON.
Thanks to [this site](https://benborgers.com/posts/google-sheets-json)
`wget https://opensheet.elk.sh/1GMLOc1OU5EQ4Ig41SA6cIUd9CohzBPxcsitecfJSolg/Sheet1`

- how to integrate leaflet with svelte
  https://imfeld.dev/writing/leaflet_with_svelte
- leaflet docs https://leafletjs.com/reference.html

- https://legacy.imagemagick.org/discourse-server/viewtopic.php?t=30987 describes how to extract the exif data using imagemagick
  call like this: `docker-compose run magick convert raw-assets/pictures/IMG_20220629_094517.jpg json:`
- converting EXIF GPS data (sexagesimal) to/from lat/lng
  https://www.fcc.gov/media/radio/dms-decimal
  the node package for converting https://github.com/Turistforeningen/dms2dec.js
- understand all exif data https://exiv2.org/tags.html
- understanding exif orientation https://jdhao.github.io/2019/07/31/image_rotation_exif_info/

- how to layout/design with images https://aws.wideinfo.org/pagedesignpro.com/wp-content/uploads/2017/12/06141237/ezio-gallery-wordpress-website-template-6.jpg
- icon set https://feathericons.com/
- I found the initial set of colors on https://uidesigndaily.com/ but not sure where
- this is one inspiration https://uidesigndaily.com/posts/photoshop-timeline-tasks-calendar-mobile-day-201
- the color palette was inspired by https://www.uidesigndaily.com/posts/sketch-booking-modal-reservation-day-1452


- icons by Cole Bemis https://github.com/feathericons/feather